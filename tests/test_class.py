import pytest

from simplex_cli.stl import \
    TransportTransaction

from simplex_cli.schemes import \
    REF_SDU_KEYS, \
    REF_SDU_VALUES

from simplex_cli.exceptions import \
    SimplexException

"""
import random

from simplex_cli.hrl import \
    SubscriberSAP, \
    HTTPRequestSAP, \
    CircuitSAP, \
    TransportSAP, \
    CarrierSAP

from simplex_cli.stl import \
    SimplexTransaction, \
    PingTransaction, \
    SubscriberTransaction, \
    CircuitTransaction, \
    CarrierTransaction, \
    TransportTransaction
"""

hostport = '192.168.95.59:3000'
scheme = 'http'

opt = [
{
    "type": REF_SDU_VALUES['TransportSapiType'],
    "id": 0,  # This field does the trick if it is greater than 0
    "match-field": '',
    "match-expr": '',  # regular expression
    "template": '',
    "tref": 'Orange',
    "force-prompt": False
    },
{
    "type": REF_SDU_VALUES['TransportSapiType'],
    "id": 0,  # This field does the trick if it is greater than 0
    "match-field": '',
    "match-expr": '',  # regular expression
    "template": '',
    "tref": 'NEBA',
    "force-prompt": False
    }
]


@pytest.mark.parametrize('id', opt)
def test_transport_create(id):
    try:
        _stl = TransportTransaction(hostport=hostport, scheme=scheme, options=id)
        _stl.create()
        print(' > Transpport validated tref={}'.format(id['tref']))

    except SimplexException as e:
        print(' > Scheme validation exception msg={}'.format(str(e)))

    del _stl


"""

# random_id = random.sample(range(0, 100), 3)
random_id = (74, 74)
# random_cid = (309, 310, 311)

random_ip = ['45.150.184.{}'.format(str(x)) for x in random_id]
random_nas = ['i{:04d}c0'.format(x) for x in random_id]
random_data = [{'firstname': 'MyFirst_{}'.format(str(x)), 'lastname': 'MyLast_{}'.format(str(x))} for x in random_id]

opt = [
{
  "type": "subscriber",
  "id": 0,  # This field takes effect if it is greater than 0
  "match-field": "lastname",
  "match-expr": ".*rre"  # regular expression
},
{
  "type": "circuit",
  "id": 0,  # This field takes effect if it is greater than 0
  "match-field": "ipv4",
  "match-expr": ".*3"  # regular expression
},
{
  "type": "ping",
  "id": 0,  # This field takes effect if it is greater than 0
  "match-field": "",
  "match-expr": ""  # regular expression
},
{
  "type": "carrier",
  "id": 0,  # This field takes effect if it is greater than 0
  "match-field": "name",
  "match-expr": ".*S"  # regular expression
},
{
  "type": "transport",
  "id": 0,  # This field takes effect if it is greater than 0
  "match-field": "sn-auth",
  "match-expr": ".*2"  # regular expression
}
]

unit_opt = [
{
  'type': 'ping',
  'id': 0,  # This field takes effect if it is greater than 0
  'match-field': '',
  'match-expr': ''  # regular expression: .* to match in any position. Use ^<>$ to match all the string
# By defualt the value must match from the begining of the string
}
]


@pytest.mark.parametrize('opt', opt)
def test_stl(opt):
    if opt[REF_SDU_KEYS['Type']] == REF_SDU_VALUES['SubscriberSapiType']:
        _stl = SubscriberTransaction(hostport=hostport, scheme=scheme, options=opt)
    elif opt[REF_SDU_KEYS['Type']] == REF_SDU_VALUES['CircuitSapiType']:
        _stl = CircuitTransaction(hostport=hostport, scheme=scheme, options=opt)
    elif opt[REF_SDU_KEYS['Type']] == REF_SDU_VALUES['TransportSapiType']:
        _stl = TransportTransaction(hostport=hostport, scheme=scheme, options=opt)
    elif opt[REF_SDU_KEYS['Type']] == REF_SDU_VALUES['CarrierSapiType']:
        _stl = CarrierTransaction(hostport=hostport, scheme=scheme, options=opt)
    elif opt[REF_SDU_KEYS['Type']] == REF_SDU_VALUES['PingSapiType']:
        _stl = PingTransaction(hostport=hostport, scheme=scheme, options=opt)
    else:
        _stl = SimplexTransaction(hostport=hostport, scheme=scheme, options=opt)

    try:
        # _stl._get_entry_list()
        if _stl.entry_list:
            print(' > STL unexception entry={}'.format(str(_stl.entry_list)))
        else:
            print(' > STL unexception: no matched entries')

        _stl.print_table()
        # assert _hrl_sap.received_sdu['id'] == id
        # cid = _hrl_sap.received_sdu['circuits'][0]['id']
        # test_circuit_get_sap(cid)

    except SimplexHTTPError as e:
        print('> STL exception statuscode={} mesg={}'.format(str(e.status_code), str(e.body)))
    except SimplexException as e:
        print('> STL exception mesg={}'.format(str(e.message)))

    del _stl


@pytest.mark.parametrize('id', random_id)
def test_subscriber_get_sap(id):
    _hrl_sap = SubscriberSAP(hostport=hostport, scheme=scheme)
    try:
        _hrl_sap.get(match_field='firstname', match_expr='Pablo')
        print('> Subscriber unexception id={}'.format(str(id)))
        # assert _hrl_sap.received_sdu['id'] == id
        # cid = _hrl_sap.received_sdu['circuits'][0]['id']
        # test_circuit_get_sap(cid)

    except SimplexHTTPError as e:
        print('> Subscriber exception id={} mesg={}'.format(str(id), str(_hrl_sap.received_sdu['error']['message'])))
        assert e.status_code == _hrl_sap.status_code
        assert _hrl_sap.status_code == _hrl_sap.received_sdu['error']['statusCode']
        assert e.method == _hrl_sap.method
        assert e.body == _hrl_sap.received_sdu
        assert e.message == _hrl_sap.received_sdu['error']['message']

    del _hrl_sap


@pytest.mark.parametrize('id', random_nas)
def test_circuit_get_sap(id):
    _hrl_sap = CircuitSAP(hostport=hostport, scheme=scheme)
    try:
        _hrl_sap.get(npid=id)
        assert _hrl_sap.url == 'http://192.168.95.59:3000/radius/subscription/circuit?nasId={}'.format(id)
        assert _hrl_sap.received_sdu[0]['nasId'] == id
        cid = _hrl_sap.received_sdu[0]['id']
        sid = _hrl_sap.received_sdu[0]['subscription']['id']
        test_circuit_patch_command_sap(sid, cid, 'disable')

    except SimplexHTTPError as e:
        assert _hrl_sap.url == 'http://192.168.95.59:3000/radius/subscription/circuit?nasId={}'.format(id)
        assert e.status_code == _hrl_sap.status_code
        assert _hrl_sap.status_code == _hrl_sap.received_sdu['error']['statusCode']
        assert e.method == _hrl_sap.method
        assert e.body == _hrl_sap.received_sdu
        assert e.message == _hrl_sap.received_sdu['error']['message']

    del _hrl_sap


def test_circuit_patch_command_sap(sid, cid, cmd):
    _hrl_sap = CircuitSAP(hostport=hostport, scheme=scheme)
    try:
        _hrl_sap.patch(id=sid, cid=cid, cmd=cmd)
        print('> Circuit unexception id={} command={}'.format(str(cid), str(cmd)))
        # assert _hrl_sap.url == 'http://192.168.95.59:3000/radius/subscription/circuit?nasId={}'.format(id)
        # assert _hrl_sap.received_sdu['nasId'] == id

    except SimplexHTTPError as e:
        print('> Circuit modified with exception id={} mesg={}'.format(str(cid),
                                                                       str(_hrl_sap.received_sdu['error']['message'])))
        assert e.status_code == _hrl_sap.status_code
        assert _hrl_sap.status_code == _hrl_sap.received_sdu['error']['statusCode']
        assert e.method == _hrl_sap.method
        assert e.body == _hrl_sap.received_sdu
        assert e.message == _hrl_sap.received_sdu['error']['message']

    del _hrl_sap


# Delete a list of circuits and users
@pytest.mark.parametrize('sid', random_id)
# @pytest.mark.parametrize('cid', random_cid)
def test_circuit_delete_sap(sid, cid):
    _hrl_sap = CircuitSAP(hostport=hostport, scheme=scheme)
    try:
        _hrl_sap.delete(id=sid, cid=cid)
        print('> Circuit deleted with unexception id={}'.format(str(sid)))
        # assert _hrl_sap.received_sdu['id'] == id
        # cid = _hrl_sap.received_sdu['circuits'][0]['id']
        # test_subscriber_delete_sap(sid)

    except SimplexHTTPError as e:
        print('> Circuit deleted with exception id={} mesg={}'.format(str(cid),
                                                                      str(_hrl_sap.received_sdu['error']['message'])))
        assert e.status_code == _hrl_sap.status_code
        assert _hrl_sap.status_code == _hrl_sap.received_sdu['error']['statusCode']
        assert e.method == _hrl_sap.method
        assert e.body == _hrl_sap.received_sdu
        assert e.message == _hrl_sap.received_sdu['error']['message']

    del _hrl_sap


@pytest.mark.parametrize('id', random_id)
def test_subscriber_delete_sap(id):
    _hrl_sap = SubscriberSAP(hostport=hostport, scheme=scheme)
    try:
        _hrl_sap.delete(id=id)
        print('> Subscriber deleted with unexception id={}'.format(str(id)))
        # assert _hrl_sap.received_sdu['id'] == id
        # cid = _hrl_sap.received_sdu['circuits'][0]['id']
        # test_circuit_get_sap(cid)

    except SimplexHTTPError as e:
        print('> Subscriber deleted with exception id={} mesg={}'.format(str(id),
                                                                         str(_hrl_sap.received_sdu['error']['message'])))
        assert e.status_code == _hrl_sap.status_code
        assert _hrl_sap.status_code == _hrl_sap.received_sdu['error']['statusCode']
        assert e.method == _hrl_sap.method
        assert e.body == _hrl_sap.received_sdu
        assert e.message == _hrl_sap.received_sdu['error']['message']

    del _hrl_sap


@pytest.mark.parametrize('id', random_id)
def test_transport_get_sap(id):
    _hrl_sap = TransportSAP(hostport=hostport, scheme=scheme)
    try:
        _hrl_sap.get(id=id)
        print('> Transport unexception id={}'.format(str(id)))
        assert _hrl_sap.received_sdu['id'] == id
        cid = _hrl_sap.received_sdu['carrierId']
        print('> Transport unexception cid={}'.format(str(cid)))

    except SimplexHTTPError as e:
        print('> Transport exception id={} mesg={}'.format(str(id), str(_hrl_sap.received_sdu['error']['message'])))
        assert e.status_code == _hrl_sap.status_code
        assert _hrl_sap.status_code == _hrl_sap.received_sdu['error']['statusCode']
        assert e.method == _hrl_sap.method
        assert e.body == _hrl_sap.received_sdu
        assert e.message == _hrl_sap.received_sdu['error']['message']

    del _hrl_sap
    
"""
