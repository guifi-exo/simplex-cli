# Simplex-CLI

Simplex-CLI is a client agent suite for [Simplex-API](https://gitlab.com/guifi-exo/simplex-api).

# Description

[Simplex-API](https://gitlab.com/guifi-exo/simplex-api) is a REST API written in Javascript, developed to manage the Internet provisioning procedures of a small ISP. A first discussion about the
architecture of the service can be found [here (catalan version)](https://gitlab.com/guifi-exo/simplex-api/-/blob/master/simplex.md).

Simplex-CLI is a Python command line tool suite that can be used to apply changes on a RADIUS server database. Other actions are possible,
for instance, send PoD to NAS, request lists of data related to subscribers, connections (circuits), bearing services (transports) and carriers.

Actually Simplex-CLI is a set of tools with many features in order to perform administrative tasks:

* Add new subscriptions, circuits, carriers and transport entries
* Automatic creation of PPP connections profiles (circuits): usernames, passwords and IPv4/IPv6 addresses asigment
* Delete subscriptions, circuits, carriers and transport entries
* List of data groups related to subscriptions, circuits, carriers and transport entries
* Modify the administrative status of a circuit
* Send PoD to NAS to force de disconnection of a PPP interface

# Installation

At this moment Simplex-CLI is not yet available in [PyPI](https://pypi.org/) repository. If you want to install it in your Debian 9 or 10 distribution, follow these steps:

1. Install Git and Python3: `sudo apt-get install git python3`
2. Install Pip and Setuptools: `sudo apt-get install python-pip python3-setuptools`
3. Clone from our repository in a building folder: `git -b <tag_name> clone https://gitlab.com/guifi-exo/simplex-cli.git && cd simplex-cli`
4. Install Simplex-CLI package on your system: `sudo python3 setup.py install`

In order to install configuration data files and enable autocomplete functions under `bash` terminal, please follow these steps:

1. Go to build directory. There you will find the shell script `setup_config.sh`
2. Grant user execution permissions and run it:

```
# chmod +x ./setup_config.sh
# ./setup_config.sh
```

3. Follow the script's suggestion if you want to enable autocomplete functions.

At this moment, you should find the default configuration file under `~/.spxc` directory. Now you can customize it.

If all goes right, you should be able to run any of the simplex-CLI tools: `spxlist, spxdelete, spxcreate, spxset`

# Usage

The general invoking syntax of Simplex-CLI tools is:
```
<spxlist|spxcreate|spxdelete|spxset> [OPTIONS] COMMAND [ARGS]...
```
To invoke specific help, type:
```
<spxlist|spxcreate|spxdelete|spxset> <command_name> --help
```
Press `<TAB>` or `--<TAB>` to get autocompletion suggestions. The common available options and commands are:
```
Usage: <tool-name> [OPTIONS] COMMAND [ARGS]...

Options:
  -f, --config-file TEXT      Set specific config file
  -a, --authority TEXT        Define the URI authority
  -sh, --scheme [http|https]  Select the URI scheme in case you set authority
  -j, --show-json-resp        Enable showing JSON messages if HTTP exceptions
                              arise
  --debug / --no-debug        Enable debug mode
  --version                   Show the version and exit.
  --help                      Show this message and exit.

Commands:
  carrier     Get a list of carrier entries
  circuit     Get a list of circuit entries
  subscriber  Get a list of subscriber entries
  transport   Get a list of transport entries
```

# Configuration and template files

Simplex-CLI stores a default configuration file located in:
```
~/.spxc/spxc.cfg
```
We recommend you customize some values by editing this file. By this way you don't need to invoke the tools setting basic options every time.
As an example, `spxc.cfg` configuration file looks like this:
```
[server]
dst_port = 3000
hostname = 127.0.0.1
scheme = http
```
If the option `-f, --config-file` is included, an alternative file is parsed. By other hand, it is possible
to connect to a server just setting the authority part of the URI with `-a, --authority` and
the URI scheme with `-sh, --scheme [http|https]` separately:

```
# spxlist -a '192.168.1.100:8080' -sh http ping
```

In the case of transport templates, Simplex-CLI includes a default file located in:
```
~/.spxc/transp_default.json
```
You can create new transport templates. They can be loaded after editing the template file in the default location.

# spxlist
This tool shows data related to all entries (circuit, subscribers,...). After the command, you can filter the retrieved data from Simplex-API invoking the entry id (`--ident/-id`) or by any regular expression applied to the fields:
```
username
ipv4
ipv6
nasId
isDisabled
hasManagement
firstname
lastname
```
As example, we want to list all the circuit entries with a username that contains the string _005_ in any position:
```
spxlist circuit --match-field username --match-expr '.005'
```
If we want to list all the circuit entries with a username that contains _c1_ after an arbitrary number of characters:
```
spxlist circuit --match-field username --match-expr '.*c1'
```
# spxcreate
This tool allows you to create new data entries. If you want to create a subscriber, you can use any of these arguments:
```
--no-append-manag  --no-append-circ --firstname --lastname --ipv4
```
For instance, if you want to create a circuit and attach it to a subscriber, type this:
```
spxcreate circuit --attach-to <subs-id>
```
Other available arguments:
```
--no-append-manag --attach-to --ipv4 --ipv6 --username --password
```
If any of these arguments is empty or not specified, [Simplex-API](https://gitlab.com/guifi-exo/simplex-api) will assign new values automatically.
If you want to create a transport entry, this tool will prompt the data according to the template file `transp_default.json`. This is the default file:
```
[
  {
    "sref": "TRBE2",
    "name": "Orange",
    "location": "",
    "s_vlan":  {"sid":  "Service VLAN", "vid":  3864, "pcp":  0},
    "c_vlan":  {"sid":  "Customer VLAN", "vid":  -1, "pcp":  0},
    "r_vlan":  {"sid":  "R/S VLAN", "vid":  23, "pcp":  0},
    "endpointDev": "%Mikrotik hAPac2%",
    "eoipRemAddress": "%10.254.254.254%",
    "eoipTunId": -1
  },
  {
    "sref": "TRBE1",
    "name": "NEBA",
    "location": "",
    "s_vlan":  {"sid":  "Service VLAN", "vid":  3864, "pcp":  0},
    "c_vlan":  {"sid":  "Customer VLAN", "vid":  -1, "pcp":  0},
    "r_vlan":  {"sid":  "R/S VLAN", "vid":  24, "pcp":  3},
    "endpointDev": "%Echolife EG8010H%",
    "snAuth": "",
    "passAuth": ""
  }
]
```

In our case we have defined two bearing (transport) services called NEBA and Orange. So if you want to create one of these services attached to carrrier id _<carr-id>_, type:
```
spxcreate transport --transp-serv 'NEBA' --attach-to <carr-id>
```
After that, this tool will prompt for the desired values of the valiables present in `transp_default.json`. If the value of string variable is empty, `spxcreate` will prompt it without showing any default value. If the value of string variable is not empty, it will not prompt and take the value predefined in template file. If the value of string variable is not empty but is surrounded by `%`, it will be prompted with surrounded value as default.
Similar logic is applied to integer values. In this case, `-1` value makes that the variable value will be prompted. In other case, the value will not be prompted.

# spxdelete
This tool allows you to delete data entries. All the commands suport these common arguments to select the entries that will be deleted:
```
--ident  --match-field  --match-expr
```
It is possible to select an entry by ident or a group of entries by regular expression matching. If a single or a group of entries is matched, all of them will be deleted.

# spxset
This tool allows to modify some specific values of entries and to send executive commands. These are the available commands that can be applied under usual ident or regular expression arguments:
```
carrier    circuit    transport
```
For `carrier`, you can modify the name of carrier. For instance:
```
spxset carrier --ident <carr_id> --name 'My new name'
```
For `circuit`, you can attach a circuit to a transport entry:
```
spxset circuit --attach-to <transp_id>
```
It is also possible to send these executive commands to enable, disable and force a circuit disconnection:
```
spxset circuit --ident <circ_id> --command <TAB><TAB>
enable      disable     disconnect
```
