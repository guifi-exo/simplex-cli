import requests
import re
import json

from itertools import filterfalse

from simplex_cli.exceptions import \
    SimplexHTTPError, \
    SimplexException

from simplex_cli.constants import \
    INHERIT_EXCEP_REGEXP, \
    CONN_TIMEOUT, \
    SOCKET_REGEXP, \
    REQ_KEYS, \
    HTTP_ERROR_FIELDS, \
    HTTP_HEAD_FIELDS, \
    HTTP_HEAD_VALUES, \
    HTTP_CONT_FIELDS, \
    DEF_SCHEME

from simplex_cli.utils import \
    _logger, \
    match_field_value


class HTTPRequestSAP:
    class _HTTPExceptionCatch:
        def __call__(self, *call_ref):
            def wrapper(obj, *args, **kwargs):
                try:
                    _resp = call_ref[0](obj, *args, **kwargs)
                    obj.status_code = _resp.status_code
                    obj.method = _resp.request.method

                    if HTTP_HEAD_FIELDS['ContentType'] not in _resp.headers:
                        _logger.debug('No content-type found in HTTP headers')
                        raise SimplexHTTPError('No content-type found in HTTP headers',
                                               'Unknown Exception',
                                               '{}'.format(_resp.status_code),
                                               _resp)

                    # Review how to deal better with error codes and parsing JSON errors

                    _jresp = _resp.json()

                    if isinstance(_jresp, list):
                        _logger.debug('Retrieved response JSON list of {} items'.format(len(_jresp)))
                        obj.received_sdu = _jresp
                    else:
                        _logger.debug('Retrieved response JSON dictionary of {} keys'.format(len(_jresp)))
                        obj.received_sdu.clear()
                        obj.received_sdu.append(_jresp)

                    if _resp.status_code != requests.codes.ok:
                        _json = _jresp[HTTP_ERROR_FIELDS['Error']]

                        raise SimplexHTTPError('{}'.format(_json[HTTP_ERROR_FIELDS['Message']]
                                                           if HTTP_ERROR_FIELDS['Message'] in _json else
                                                           'Unable to retrieve exception message'),
                                               '{}'.format(_json[HTTP_ERROR_FIELDS['Name']]
                                                           if HTTP_ERROR_FIELDS['Name'] in _json else
                                                           'Unknown Exception'),
                                               '{}'.format(_json[HTTP_ERROR_FIELDS['StatusCode']]
                                                           if HTTP_ERROR_FIELDS['StatusCode'] in _json else
                                                           0),
                                               _resp)

                    elif not isinstance(_jresp, list) and HTTP_ERROR_FIELDS['Error'] in _jresp.keys():
                        _logger.debug('Status code is {} but the HTTP message body contains \'{}\''.
                                      format(_resp.status_code, HTTP_ERROR_FIELDS['Error']))
                        raise SimplexHTTPError('{} retrived from HTTP body'.format(HTTP_ERROR_FIELDS['Error']),
                                               'Error retrived in message body',
                                               '{}'.format(_resp.status_code),
                                               _resp)

                except json.JSONDecodeError as e:
                    _ename = type(e).__name__
                    raise SimplexException(str(e), _ename)

                except requests.exceptions.RequestException as e:
                    _ename = type(e).__name__

                    if _ename == 'ConnectionError' or _ename == 'ConnectTimeout':
                        for v in e.args[0].reason.args:
                            if type(v) == str:
                                # Remove trailer of characters inside <>: delimiters
                                s = re.sub(INHERIT_EXCEP_REGEXP, '', v)
                                raise SimplexException(str(s), _ename)

                        raise SimplexException('Unable to retrieve exception message', _ename)

                    if _ename == 'ReadTimeout':
                        v = e.args[0].args[0]
                        if type(v) == str:
                            # Remove trailer of characters inside <>: delimiters
                            s = re.sub(INHERIT_EXCEP_REGEXP, '', v)
                            raise SimplexException(str(s), _ename)

                        raise SimplexException('Unable to retrieve exception message', _ename)

                    if _ename == 'SSLError':
                        v = e.args[0].reason.args[0]
                        raise SimplexException(str(v), _ename)
            return wrapper

    class _ExceptionCatch:
        def __call__(self, *call_ref):
            def wrapper(*args, **kwargs):
                try:
                    call_ref[0](*args, **kwargs)
                except SimplexHTTPError as e:
                    _logger.debug(
                        'Response type: {} Status code: {} >> {}'.format(e.except_name,
                                                                         e.status_code,
                                                                         e.message))
                    raise
                except SimplexException as e:
                    _logger.debug('Simplex exception type: {} >> {}'.format(e.except_name,
                                                                            e.message))
                    raise
                except Exception as e:
                    _logger.debug('General exception type: {} >> {}'.format(str(type(e).__name__),
                                                                            str(e)))
                    raise SimplexException('Function {}() >> {}'.format(call_ref[0].__name__, str(e)),
                                           str(type(e).__name__))
            return wrapper

    class _HeaderStack:
        def __init__(self, **init_ref):
            self._ref = init_ref

        def __call__(self, *call_ref):
            if len(self._ref) > 0:
                m_field = self._ref.get(REQ_KEYS['REQ_FIELD_KEY'])
                m_value = self._ref.get(REQ_KEYS['REQ_VALUE_KEY'])

                if m_field not in HTTP_HEAD_FIELDS:
                    raise SimplexException('Header field reference \'{}\' unrecognized'.format(m_field), '')

                # If None value and these fields are passed assume some default value
                if m_field == 'Accept' and m_value is None:
                    m_value = HTTP_HEAD_VALUES['ApplicationJSON']
                elif m_field == 'ContentType' and m_value is None:
                    m_value = HTTP_HEAD_VALUES['ApplicationJSON']
                elif m_field == 'UserAgent' and m_value is None:
                    m_value = HTTP_HEAD_VALUES['SpxUserAgent']

                def wrapper(obj, **kwargs):
                    obj.headers.update({HTTP_HEAD_FIELDS[m_field]: m_value})
                    _logger.debug('Updated header {} := {}'.format(m_field, m_value))
                    return call_ref[0](obj, **kwargs)
                return wrapper
            else:
                return call_ref[0]

    class _PathBuild:
        def __call__(self, *call_ref):
            def wrapper(obj, **kwargs):
                _rt_tk = obj.root_token
                _et_tk = obj.entry_token
                _ex_tk = '{delim}{token}'
                _url = '{sch}://{auth}'.format(sch=obj.scheme, auth=obj.authority)

                if _rt_tk:
                    _url = _url + '/{}'.format(_rt_tk)

                if _et_tk:
                    _url = _url + '/{}'.format(_et_tk)

                _id = ''
                # _hd = None

                for k, v in kwargs.items():
                    if k == REQ_KEYS['REQ_IDENT_KEY'] and v > 0:
                        _id = str(v)
                    elif k == REQ_KEYS['REQ_HDATA_KEY'] and v:
                        # _hd = dict(v)
                        obj.sent_sdu = dict(v)
                    elif k == REQ_KEYS['REQ_MFIEL_KEY'] and v:
                        obj.match_field = str(v)  # This code should be removed when the API implement RE searches
                    elif k == REQ_KEYS['REQ_MVALU_KEY'] and v:
                        obj.match_expr = str(v)  # This code should be removed when the API implement RE searches
                    else:
                        _logger.debug('Key \'{}={}\' has empty value or does not match with any predefined key'.
                                      format(str(k), str(v)))

                if _id:
                    _url = _url + _ex_tk.format(delim='/', token=_id)
                else:
                    _url = _url + _ex_tk.format(delim='', token='')

                _logger.debug('Built URL: {}'.format(_url))
                obj.url = _url
                call_ref[0](obj)

            return wrapper

    def __init__(self, hostport=None, scheme=DEF_SCHEME, rt_tk='', et_tk=''):
        self.scheme = scheme
        self.authority = ''
        self.headers = dict()
        self.sent_sdu = dict()
        self.received_sdu = list()
        self.root_token = rt_tk
        self.entry_token = et_tk
        self.url = ''
        self.status_code = int()
        self.method = ''
        self.match_field = ''  # This code should be removed when the API implement RE searches
        self.match_expr = ''   # This code should be removed when the API implement RE searches

        if hostport:
            self._set_authority(hostport)
        else:
            raise SimplexException('Hostname and port value is empty', '')

        self.set_common_fields()

    @_HTTPExceptionCatch()
    def _get_request(self, headers, url, _timeout=CONN_TIMEOUT):
        return requests.get(url, headers=headers, timeout=_timeout)

    @_HTTPExceptionCatch()
    def _post_request(self, headers, url, p_data=None, _timeout=CONN_TIMEOUT):
        return requests.post(url,
                             headers=headers,
                             timeout=_timeout,
                             data=(p_data if p_data is None else json.dumps(p_data)))

    @_HTTPExceptionCatch()
    def _delete_request(self, headers, url, _timeout=CONN_TIMEOUT):
        return requests.delete(url, headers=headers, timeout=_timeout)

    @_HTTPExceptionCatch()
    def _patch_request(self, headers, url, p_data=None, _timeout=CONN_TIMEOUT):
        return requests.patch(url,
                              headers=headers,
                              timeout=_timeout,
                              data=(p_data if p_data is None else json.dumps(p_data)))

    @_ExceptionCatch()
    def _set_authority(self, hp):
        try:
            _recom = re.compile(SOCKET_REGEXP)
            _reout = _recom.match(hp)
            socket = _reout.group(1, 2)

            if not socket[1]:
                self.authority = '{}'.format(socket[0])
                _logger.debug('Server authority: {}://{}'.format(self.scheme, socket[0]))
            else:
                self.authority = '{}:{}'.format(socket[0], socket[1])
                _logger.debug('Server authority: {}://{}:{}'.format(self.scheme, socket[0], socket[1]))

        except Exception as e:
            raise SimplexException('Hostname and port parsing error: {}'.format(str(e)), type(e).__name__)

    @_HeaderStack(field='UserAgent')
    @_HeaderStack(field='Accept')
    @_HeaderStack(field='ContentType')
    def set_common_fields(self):
        pass

    @_ExceptionCatch()
    @_PathBuild()
    def get(self):
        _logger.debug('{}: Getting endpoint {}'.format(self.__class__.__name__, self.url))
        self._get_request(self.headers, self.url)

        # This code should be removed when the API implement RE searches
        if self.match_field and self.received_sdu:
            self.received_sdu = list(filterfalse(lambda k:
                                                 not match_field_value(k, self.match_field, self.match_expr),
                                                 self.received_sdu))

        if not self.received_sdu:
            raise SimplexException('No matching entries found for \'{}={}\''
                                   .format(self.match_field, self.match_expr), 'Matching Entries')
            
    @_PathBuild()
    def post(self):
        _logger.debug('{}: Posting endpoint {}'.format(self.__class__.__name__, self.url))
        self._post_request(self.headers, self.url, self.sent_sdu)

    @_PathBuild()
    def delete(self):
        _logger.debug('{}: Deleting endpoint {}'.format(self.__class__.__name__, self.url))
        self._delete_request(self.headers, self.url)

    @_PathBuild()
    def patch(self):
        _logger.debug('{}: Patching endpoint {}'.format(self.__class__.__name__, self.url))
        self._patch_request(self.headers, self.url, self.sent_sdu)


class PingSAP(HTTPRequestSAP):
    def __init__(self, hostport=None, scheme=DEF_SCHEME):
        super().__init__(hostport=hostport, scheme=scheme, rt_tk='', et_tk='ping')


class SubscriberSAP(HTTPRequestSAP):
    def __init__(self, hostport=None, scheme=DEF_SCHEME):
        super().__init__(hostport=hostport, scheme=scheme, rt_tk='radius', et_tk='subscription')


class TransportSAP(HTTPRequestSAP):
    def __init__(self, hostport=None, scheme=DEF_SCHEME):
        super().__init__(hostport=hostport, scheme=scheme, rt_tk='', et_tk='transport')


class CarrierSAP(HTTPRequestSAP):
    def __init__(self, hostport=None, scheme=DEF_SCHEME):
        super().__init__(hostport=hostport, scheme=scheme, rt_tk='', et_tk='carrier')


class CircuitSAP(HTTPRequestSAP):
    class _ExceptionCatch(HTTPRequestSAP._ExceptionCatch):
        pass

    class _PathBuild:
        def __call__(self, *call_ref):
            def wrapper(obj, **kwargs):
                _rt_tk = obj.root_token
                _et_tk = obj.entry_token
                _se_tk = obj.subentry_token
                _ss_tk = obj.subsubentry_token
                _ex_tk = '{delim}{token}'
                _url = '{sch}://{auth}'.format(sch=obj.scheme, auth=obj.authority)

                if _rt_tk:
                    _url = _url + '/{}'.format(_rt_tk)

                if _et_tk:
                    _url = _url + '/{}'.format(_et_tk)

                _id = ''
                _si = ''
                _ni = ''
                _ip = ''
                # _hd = None
                _cm = ''
                _ti = ''
                _dt = False

                for k, v in kwargs.items():
                    if k == REQ_KEYS['REQ_IDENT_KEY'] and v > 0:
                        _id = str(v)
                    elif k == REQ_KEYS['REQ_SBEID_KEY'] and v >= 0:
                        _si = str(v)
                    elif k == REQ_KEYS['REQ_QNPID_KEY'] and v:
                        _ni = str(v)
                    elif k == REQ_KEYS['REQ_IP4ID_KEY'] and v:
                        _ip = str(v)
                    elif k == REQ_KEYS['REQ_EPCOM_KEY'] and v:
                        _cm = str(v)
                    elif k == REQ_KEYS['REQ_TRPID_KEY'] and v >= 0:
                        _ti = str(v)
                    elif k == REQ_KEYS['REQ_DELTR_KEY'] and v:
                        _dt = bool(v)
                    elif k == REQ_KEYS['REQ_HDATA_KEY'] and v:
                        obj.sent_sdu = dict(v)
                    elif k == REQ_KEYS['REQ_MFIEL_KEY'] and v:
                        obj.match_field = str(v)  # This code should be removed when the API implement RE searches
                    elif k == REQ_KEYS['REQ_MVALU_KEY'] and v:
                        obj.match_expr = str(v)  # This code should be removed when the API implement RE searches
                    else:
                        _logger.debug('Key \'{}={}\' has empty value or does not match with any predefined key'.
                                      format(str(k), str(v)))

                if _id:
                    if _si:
                        _url = _url + _ex_tk.format(delim='/', token=_si)
                        _url = _url + _ex_tk.format(delim='/', token=_se_tk)
                        _url = _url + _ex_tk.format(delim='/', token=_id)
                        if _cm:
                            _url = _url + _ex_tk.format(delim='/', token=_cm)
                        elif _ti:
                            _url = _url + _ex_tk.format(delim='/', token=_ss_tk)
                            _url = _url + _ex_tk.format(delim='/', token=_ti)
                        elif _dt:
                            _url = _url + _ex_tk.format(delim='/', token=_ss_tk)
                    else:
                        _url = _url + _ex_tk.format(delim='/', token=_se_tk)
                        _url = _url + _ex_tk.format(delim='/', token=_id)
                elif _si:
                    _url = _url + _ex_tk.format(delim='/', token=_si)
                    _url = _url + _ex_tk.format(delim='/', token=_se_tk)
                else:
                    _url = _url + _ex_tk.format(delim='/', token=_se_tk)
                    if _ni:
                        _url = _url + _ex_tk.format(delim='?', token=HTTP_CONT_FIELDS['NasPortId'])
                        _url = _url + _ex_tk.format(delim='=', token=_ni)
                    if _ip:
                        _del = '{}'.format('&' if _ni else '?')
                        _url = _url + _ex_tk.format(delim=_del, token=HTTP_CONT_FIELDS['IPv4'])
                        _url = _url + _ex_tk.format(delim='=', token=_ip)

                _logger.debug('Built URL: {}'.format(_url))
                obj.url = _url
                # obj.sent_sdu = _hd
                call_ref[0](obj)

            return wrapper

    def __init__(self, hostport=None, scheme=DEF_SCHEME):
        super().__init__(hostport=hostport, scheme=scheme, rt_tk='radius', et_tk='subscription')
        self.subentry_token = 'circuit'
        self.subsubentry_token = 'transport'

    @_ExceptionCatch()
    @_PathBuild()
    def get(self):
        _logger.debug('{}: Getting endpoint {}'.format(self.__class__.__name__, self.url))
        self._get_request(self.headers, self.url)

        # This code should be removed when the API implement RE searches
        if self.match_field and self.received_sdu:
            self.received_sdu = list(filterfalse(lambda k:
                                                 not match_field_value(k, self.match_field, self.match_expr),
                                                 self.received_sdu))

        if not self.received_sdu:
            raise SimplexException('No matching entries were found for {}={}'
                                   .format(self.match_field, self.match_expr), 'Entries Matching')

    @_PathBuild()
    def post(self):
        _logger.debug('{}: Posting endpoint {}'.format(self.__class__.__name__, self.url))
        self._post_request(self.headers, self.url, self.sent_sdu)

    @_PathBuild()
    def delete(self):
        _logger.debug('{}: Deleting endpoint {}'.format(self.__class__.__name__, self.url))
        self._delete_request(self.headers, self.url)

    @_PathBuild()
    def patch(self):
        _logger.debug('{}: Patching endpoint {}'.format(self.__class__.__name__, self.url))
        self._patch_request(self.headers, self.url, self.sent_sdu)
