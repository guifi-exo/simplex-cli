import click

from simplex_cli import \
    __version__, \
    __cli_name__
from simplex_cli.constants import \
    DEF_SCHEME, \
    SCHEME_OPTS
from simplex_cli.settings import \
    get_config_uri
from simplex_cli.utils import \
    setup_logging, \
    _logger

from simplex_cli.exceptions import \
    SimplexException, \
    SimplexHTTPError

from simplex_cli.stl import \
    SubscriberTransaction, \
    CircuitTransaction, \
    CarrierTransaction, \
    TransportTransaction

from simplex_cli.schemes import \
    REF_SDU_VALUES


@click.group(invoke_without_command=False)
@click.option('-f', '--config-file', 'cfg', type=str, default='',
              help='Set specific config file')
@click.option('-a', '--authority', 'hostp', default='', type=str,
              help='Define the URI authority')
@click.option('-sh', '--scheme', 'sche', type=click.Choice(SCHEME_OPTS), default=DEF_SCHEME,
              help='Select the URI scheme in case you set authority')
@click.option('-j', '--show-json-resp', 'jrep', flag_value=True,
              help='Enable showing JSON messages if HTTP exceptions arise')
@click.option('--debug/--no-debug', default=False,
              help='Enable debug mode')
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx, debug, cfg, hostp, sche, jrep):

    ctx.ensure_object(dict)

    # if ctx.invoked_subcommand is None:
    #     click.secho('You have to specify one command. Set --help option to get more information.')
    #     cli.exit()

    ctx.obj['DEBUG'] = debug

    if ctx.obj['DEBUG']:
        setup_logging('DEBUG')
    else:
        setup_logging('INFO')

    if not hostp:
        try:
            uri_tokens = get_config_uri(cfg)
            hostp = '{}:{}'.format(uri_tokens[1], uri_tokens[2])
            sche = uri_tokens[0]
        except SimplexException as e:
            e.print_error_resp()
            ctx.abort()

    ctx.obj['SCHEME'] = sche
    ctx.obj['HOSTPORT'] = hostp
    ctx.obj['JSONRESP'] = jrep

    click.secho('Building request to SAPI server authority {scheme}://{auth}'.format(scheme=sche,
                                                                                     auth=hostp), fg='white')

    _logger.debug('Context variables: SCHEME = {}, HOSTPORT = {}, JSONRESP = {}, '.format(ctx.obj['SCHEME'],
                                                                                          ctx.obj['HOSTPORT'],
                                                                                          ctx.obj['JSONRESP']))


@cli.command(name='subscriber')
@click.option('-nm', '--no-append-manag', 'noaman', is_flag=True,
              help='No append a management circuit to a new user\'s circuit')
@click.option('-nc', '--no-append-circ', 'noacir', is_flag=True,
              help='No append a circuit to a new user')
@click.option('-fn', '--firstname', 'frnam', type=str, default='DefaultFirstName',
              help='Specify firstname of the new subscriber')
@click.option('-ln', '--lastname', 'lsnam', type=str, default='',
              help='Specify lastname of the new subscriber')
@click.option('-ip4', '--ipv4', 'ip4', type=str, default='',
              help='Specify an IPv4 to Framed-IP-Address attribute')
@click.pass_context
def subscriber(ctx, noaman, noacir, frnam, lsnam, ip4):
    """Create a subscriber entry"""

    click.secho('Creating a subscriber')

    """The keys of options dict come from REQ_KEYS"""

    _data = {
        "firstname": frnam,
        "lastname": lsnam,
        "append-circ": not noacir,
        "append-manag": not noaman,
        "ipv4": ip4
    }

    opt = {
            "type": REF_SDU_VALUES['SubscriberSapiType'],
            "id": 0,  # This field does the trick if it is greater than 0
            "match-field": '',
            "match-expr": '',  # regular expression
            "data": _data
        }

    try:
        _stl = SubscriberTransaction(hostport=ctx.obj['HOSTPORT'],
                                     scheme=ctx.obj['SCHEME'],
                                     quite=True,  # If True the Transaction class does not request an initial
                                     # get request
                                     options=opt)
        _stl.create()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


@cli.command(name='circuit')
@click.pass_context
@click.option('-nm', '--no-append-manag', 'noaman', is_flag=True,
              help='No append a management circuit to new circuit')
@click.option('-a', '--attach-to', 'ident', required=True, type=int,
              help='Attach new circuit to this subscriber\'s id')
@click.option('-ip4', '--ipv4', 'ip4', type=str, default='',
              help='Specify an IPv4 to Framed-IP-Address attribute')
@click.option('-ip6', '--ipv6', 'ip6', type=str, default='',
              help='Specify an IPv6 prefix to Delegated-IPv6-Prefix attribute')
@click.option('-un', '--username', 'uname', type=str, default='',
              help='Specify a username to new circuit')
@click.option('-ps', '--password', 'passwd', type=str, default='',
              help='Specify a password to new circuit')
def circuit(ctx, ident, noaman, ip4, ip6, uname, passwd):
    """Create a circuit to a list of subscriber entries"""

    click.secho('Creating a circuit')

    """The keys of options dict come from REQ_KEYS"""

    _data = {
        "append-manag": not noaman,
        "ipv4": ip4,
        "ipv6": ip6,
        "username": uname,
        "password": passwd
    }

    opt = {
            "type": REF_SDU_VALUES['CircuitSapiType'],
            "id": 0,  # This field does the trick if it is greater than 0
            "match-field": '',
            "match-expr": '',  # regular expression
            "subs": ident,
            "data": _data
        }

    try:
        _stl = CircuitTransaction(hostport=ctx.obj['HOSTPORT'],
                                  scheme=ctx.obj['SCHEME'],
                                  quite=True,  # If True the Transaction class does not request an initial get request
                                  options=opt)
        _stl.create()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


@cli.command(name='carrier')
@click.pass_context
@click.option('-n', '--name', 'name', type=str, required=True,
              help='Specify the name of the carrier')
def carrier(ctx, name):
    """Create a new carrier entry"""

    click.secho('Creating a carrier')

    """The keys of options dict come from REQ_KEYS"""

    _data = {
        "name": name
    }

    opt = {
            "type": REF_SDU_VALUES['CarrierSapiType'],
            "id": 0,  # This field does the trick if it is greater than 0
            "match-field": '',
            "match-expr": '',  # regular expression
            "data": _data
        }

    try:
        _stl = CarrierTransaction(hostport=ctx.obj['HOSTPORT'],
                                  scheme=ctx.obj['SCHEME'],
                                  quite=True,  # If True the Transaction class does not request an initial get request
                                  options=opt)
        _stl.create()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


@cli.command(name='transport')
@click.pass_context
@click.option('-ac', '--attach-to', 'crid', required=True, type=int,
              help='Attach new transport to this carrier\'s id')
@click.option('-te', '--template-file', 'temp', type=str, default='',
              help='Specify the template file to be based prompt')
@click.option('-ts', '--transp-serv', 'tref', type=str, required=True,
              help='Specify the name of the transport service to be applied')
@click.option('-fp', '--force-prompt', 'fprom', is_flag=True,
              help='Force prompt all the fields present on applied template')
def transport(ctx, crid, temp, tref, fprom):
    """Create a new transport entry"""

    click.secho('Creating a transport entry')

    """The keys of options dict come from REQ_KEYS"""

    opt = {
            "type": REF_SDU_VALUES['TransportSapiType'],
            "id": 0,  # This field does the trick if it is greater than 0
            "match-field": '',
            "match-expr": '',  # regular expression
            "attach-to": crid,
            "template": temp,
            "tref": tref,
            "force-prompt": fprom
        }

    try:
        _stl = TransportTransaction(hostport=ctx.obj['HOSTPORT'],
                                    scheme=ctx.obj['SCHEME'],
                                    quite=True,  # If True the Transaction class does not request an initial get request
                                    options=opt)
        _stl.create()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


if __name__ == '__main__':
    cli(obj={})
