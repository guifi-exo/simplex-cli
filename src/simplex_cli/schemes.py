from simplex_cli.constants import \
  HTTP_CONT_FIELDS, \
  SNAUTH_REGEXP, \
  PSAUTH_REGEXP

REF_SDU_KEYS = {
  'SubsInstance': 'subs',
  'NasId': 'nasId',
  'Ipv4': 'ipv4',
  'Ipv6': 'ipv6',
  'Username': 'username',
  'Password': 'password',
  'Id': 'id',
  'PayLoad': 'data',
  'Type': 'type',
  'MatchField': 'match-field',
  'MatchExpr': 'match-expr',
  'ShowSecret': 'show-secret',
  'AppendCirc': 'append-circ',
  'AppendManaged': 'append-manag',
  'AttachToCarrier': 'attach-to',
  'CarrierName': 'name',
  'ForcePrompt': 'force-prompt',
  'DataSubsFirstName': 'firstname',
  'DataSubsLastName': 'lastname'
}

REF_SDU_VALUES = {
  'SubscriberSapiType': 'subscriber',
  'CircuitSapiType': 'circuit',
  'TransportSapiType': 'transport',
  'CarrierSapiType': 'carrier',
  'PingSapiType': 'ping',
  'DataSubsFirstname': 'firstname',
  'DataSubsLastname': 'lastname',
  'DataCircManaged': 'hasManagement',
  'DataInstCommand': 'command',
  'DataInstEnable': 'enable',
  'DataInstDisable': 'disable',
  'DataInstDisconn': 'disconnect'
}

TEMPL_SREF_KEY = HTTP_CONT_FIELDS['TransportSref']
TEMPL_TREF_KEY = HTTP_CONT_FIELDS['TransportName']
TEMPL_TRBE1_VAL = "TRBE1"
TEMPL_TRBE2_VAL = "TRBE2"


BEARER_VLAN = {
  "type": "object",
  "properties": {
    "sid": {
      "type": "string",
      "enum": ["Service VLAN", "Customer VLAN", "R/S VLAN"]
    },
    "vid": {
      "type": "integer",
      "minimum": -1,
      "maximum": 4095
    },
    "pcp": {
      "type": "integer",
      "minimum": -1,
      "maximum": 7
      }
    }
  }

TRBE1_SCHEME = {
  "$id": "https://gitlab.com/guifi-exo/simplex-api",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "required": [
    TEMPL_SREF_KEY,
    TEMPL_TREF_KEY,
    "location",
    "s_vlan",
    "c_vlan",
    "r_vlan",
    "endpointDev",
    "snAuth",
    "passAuth"
  ],
  "properties": {
    TEMPL_SREF_KEY: {
      "type": "string",
      "const": TEMPL_TRBE1_VAL
    },
    TEMPL_TREF_KEY: {
      "type": "string"
    },
    "location": {
      "type": "string"
    },
    "s_vlan": BEARER_VLAN,
    "c_vlan": BEARER_VLAN,
    "r_vlan": BEARER_VLAN,
    "endpointDev": {
      "type": "string"
    },
    "snAuth": {
      "type": "string",
      "pattern": SNAUTH_REGEXP
    },
    "passAuth": {
      "type": "string",
      "pattern": PSAUTH_REGEXP
    }
  }
}

TRBE2_SCHEME = {
  "$id": "https://gitlab.com/guifi-exo/simplex-api",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "required": [
    TEMPL_SREF_KEY,
    TEMPL_TREF_KEY,
    "location",
    "s_vlan",
    "c_vlan",
    "r_vlan",
    "endpointDev",
    "eoipRemAddress",
    "eoipTunId"
  ],
  "properties": {
    TEMPL_SREF_KEY: {
      "type": "string",
      "const": TEMPL_TRBE2_VAL
    },
    TEMPL_TREF_KEY: {
      "type": "string"
    },
    "location": {
      "type": "string"
    },
    "s_vlan": BEARER_VLAN,
    "c_vlan": BEARER_VLAN,
    "r_vlan": BEARER_VLAN,
    "endpointDev": {
      "type": "string"
    },
    "eoipRemAddress": {
      "type": "string"
    },
    "eoipTunId": {
      "type": "integer",
      "minimum": -1,
      "maximum": 4095
    }
  }
}
