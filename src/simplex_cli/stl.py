import click
import json
import jsonschema

from simplex_cli.hrl import \
    SubscriberSAP, \
    CircuitSAP, \
    TransportSAP, \
    CarrierSAP, \
    PingSAP

from simplex_cli.exceptions import \
    SimplexHTTPError, \
    SimplexException

from simplex_cli.constants import \
    DEF_SCHEME, \
    HTTP_CONT_FIELDS, \
    REQ_KEYS, \
    PRINT_FORMAT_FIELDS

from simplex_cli.schemes import \
    REF_SDU_KEYS, \
    REF_SDU_VALUES, \
    TRBE1_SCHEME, \
    TRBE2_SCHEME, \
    TEMPL_SREF_KEY, \
    TEMPL_TRBE1_VAL, \
    TEMPL_TRBE2_VAL, \
    TEMPL_TREF_KEY

from simplex_cli.utils import \
    _logger, \
    print_row, \
    get_form_row, \
    scheme_int_prompt,\
    scheme_string_prompt

from simplex_cli.settings import \
    get_transp_settings


class SimplexTransaction:
    """Catch general exceptions in Simplex standard form"""
    class ExceptionCatch:
        def __call__(self, *call_ref):
            def wrapper(*args, **kwargs):
                try:
                    call_ref[0](*args, **kwargs)
                except SimplexHTTPError:
                    raise
                except SimplexException:
                    raise
                except jsonschema.exceptions.ValidationError as e:
                    _logger.debug('JSONschema exception type: {} >> {}'.format(str(type(e).__name__), str(e.message)))
                    raise SimplexException('Function {}() >> {}'.format(call_ref[0].__name__, str(e.message)),
                                           str(type(e).__name__))
                except Exception as e:
                    _logger.debug('General exception type: {} >> {}'.format(str(type(e).__name__), str(e)))
                    raise SimplexException('Function {}() >> {}'.format(call_ref[0].__name__, str(e)),
                                           str(type(e).__name__))
            return wrapper

    """Prompt a check question before to execute the inner method"""
    class CheckPrompt:
        def __init__(self, **init_ref):
            self._ref = init_ref

        def __call__(self, *call_ref):
            m_value = ''
            e_value = ''

            if len(self._ref) > 0:
                m_value = self._ref.get(REQ_KEYS['REQ_CHPRO_KEY'])
                e_value = self._ref.get(REQ_KEYS['REQ_CHEXC_KEY'])

            def wrapper(obj, **kwargs):
                if obj.entry_list:
                    click.secho('Applying options = {}'.format(json.dumps(obj.options)), bold=True)
                    click.secho(m_value)
                    obj.print_table()
                else:
                    click.secho('Entry list is empty. Nothing to do')
                    return

                if click.confirm(click.style('Are you sure?', fg='white')):
                    return call_ref[0](obj, **kwargs)

                raise SimplexException(e_value, 'Check Prompt')

            return wrapper

    def __init__(self, hostport=None, scheme=DEF_SCHEME, quite=False, options=None):
        self.scheme = scheme
        self.hostport = hostport
        self.options = dict()
        self.built_data = dict()
        self.over_templ_sref = ()
        self.print_header = bool

        if options:
            self._set_options(options)
        else:
            raise SimplexException('Option\'s class argument is empty', '')

        if not quite:
            self.entry_list = list()
            self._get_entry_list()

    def _set_options(self, opt):
        self.options = opt

    """Build a list of entries that match to fields and values"""
    def _get_entry_list(self):
        _tp = ''
        _id = 0
        _mf = ''
        _mv = ''

        for k, v in self.options.items():
            if k == REF_SDU_KEYS['Type'] and v:
                _tp = str(v)
            elif k == REF_SDU_KEYS['Id'] and v > 0:
                _id = int(v)
            elif k == REF_SDU_KEYS['MatchField'] and v:
                _mf = str(v)
            elif k == REF_SDU_KEYS['MatchExpr'] and v:
                _mv = str(v)
            else:
                _logger.debug('Key \'{}={}\' has empty value or does not match with any predefined key'.
                              format(str(k), str(v)))

        if _tp == REF_SDU_VALUES['SubscriberSapiType']:
            _hrl_sap = SubscriberSAP(self.hostport, self.scheme)
        elif _tp == REF_SDU_VALUES['CircuitSapiType']:
            _hrl_sap = CircuitSAP(self.hostport, self.scheme)
        elif _tp == REF_SDU_VALUES['TransportSapiType']:
            _hrl_sap = TransportSAP(self.hostport, self.scheme)
        elif _tp == REF_SDU_VALUES['CarrierSapiType']:
            _hrl_sap = CarrierSAP(self.hostport, self.scheme)
        elif _tp == REF_SDU_VALUES['PingSapiType']:
            _hrl_sap = PingSAP(self.hostport, self.scheme)
        else:
            raise SimplexException('Option \'{}:{}\' does not match with any predefined value'.
                                   format(str(REF_SDU_KEYS['Type']), str(_tp)), '')

        _hrl_sap.get(id=_id, match_field=_mf, match_expr=_mv)
        self.entry_list = list(_hrl_sap.received_sdu)
        del _hrl_sap
    
    """Build data structures recursively based on a template"""
    def _build_from_void_dict(self, curr_dict=None, parent_key='', force_prompt=False):
        if not curr_dict:
            raise SimplexException('The template dict is empty', 'Data building')

        curr_key_list = list(curr_dict.keys())

        for i in sorted(curr_key_list):
            curr_item = curr_dict[i]
            if type(curr_item) is str:
                scheme_string_prompt(curr_item, curr_dict, i, parent_key, force_prompt, self.over_templ_sref)
            elif type(curr_item) is int:
                scheme_int_prompt(curr_item, curr_dict, i, parent_key, force_prompt, self.over_templ_sref)
            elif type(curr_item) is dict:
                self._build_from_void_dict(curr_item, i)
            elif type(curr_item) is list:
                self._build_from_void_list(curr_item, i)

    """Build data structures recursively based on a template"""
    def _build_from_void_list(self, curr_list, curr_list_key, force_prompt=False):
        for i in curr_list:
            if type(i) is dict:
                self._build_from_void_dict(i, curr_list_key, force_prompt)

    """Build data structures recursively based on a template"""
    def _build_from_dict(self, curr_dict=None, refe_dict=None, parent_key='', force_prompt=False):
        if not curr_dict:
            self._build_from_void_dict(curr_dict, parent_key, force_prompt)
            return

        if refe_dict is None:
            refe_dict = self.built_data

        if not curr_dict:
            self._build_from_void_dict()

        refe_key_list = list(refe_dict.keys())
        curr_key_list = list(curr_dict.keys())

        for i in sorted(refe_key_list):
            for j in sorted(curr_key_list):
                if i == j:
                    refe_item = refe_dict[i]
                    curr_item = curr_dict[j]
                    if type(refe_item) is type(curr_item):
                        if type(curr_item) is str:
                            scheme_string_prompt(curr_item, curr_dict, j, parent_key, force_prompt,
                                                 self.over_templ_sref)
                        elif type(curr_item) is int:
                            scheme_int_prompt(curr_item, curr_dict, j, parent_key, force_prompt,
                                              self.over_templ_sref)
                        elif type(curr_item) is dict:
                            self._build_from_dict(curr_item, refe_item, j, force_prompt)
                        elif type(curr_item) is list:
                            self._build_from_list(curr_item, refe_item, j, i, force_prompt)

                    else:
                        _logger.error('Mismatch error between {} and {}'.format(str(i), str(j)))

                    curr_key_list.remove(j)

    """Build data structures recursively based on a template"""
    def _build_from_list(self, curr_list, refe_list, curr_list_key, refe_list_key, force_prompt=False):
        if curr_list_key != refe_list_key:
            _logger.debug('Input lists have different key references: {} / {}'.format(str(curr_list_key),
                                                                                    str(refe_list_key)))
            return

        for i in refe_list:
            for j in curr_list:
                if type(i) is dict and type(j) is dict:
                    self._build_from_dict(j, i, curr_list_key, force_prompt)
                else:
                    _logger.error('Mismatch error between {} and {}'.format(str(i), str(j)))


class PingTransaction(SimplexTransaction):
    class _ExceptionCatch(SimplexTransaction.ExceptionCatch):
        pass

    def __init__(self, hostport=None, scheme=DEF_SCHEME, quite=False, options=None):
        super().__init__(hostport=hostport, scheme=scheme, quite=quite, options=options)

    @_ExceptionCatch()
    def print_table(self, headcol='white', rowcol='blue'):
        _col_fields = '{greeting}{date}{url}'

        _d = get_form_row(date='Date', greeting='Greeting', url='URL')
        print_row(_d, _col_fields, headcol, bold=True)

        _url = '{}://{}{}'.format(self.scheme,
                                  self.entry_list[0]['headers'][HTTP_CONT_FIELDS['Host']],
                                  self.entry_list[0][HTTP_CONT_FIELDS['Path']])

        _d = get_form_row(date=self.entry_list[0][HTTP_CONT_FIELDS['Date']],
                          greeting=self.entry_list[0][HTTP_CONT_FIELDS['Greeting']],
                          url=_url)

        print_row(_d, _col_fields, rowcol, bold=False)


class SubscriberTransaction(SimplexTransaction):
    class _ExceptionCatch(SimplexTransaction.ExceptionCatch):
        pass

    class _CheckPrompt(SimplexTransaction.CheckPrompt):
        pass

    def __init__(self, hostport=None, scheme=DEF_SCHEME, quite=False, options=None):
        super().__init__(hostport=hostport, scheme=scheme, quite=quite, options=options)

    """Print a complete table of entries from a previous built entry dictionary"""
    @_ExceptionCatch()
    def print_table(self, headcol='white', rowcol='blue'):
        _col_fields = '{subs_id} {firstname}{lastname}'

        _d = get_form_row(subs_id='Subs.Id',
                          firstname='Firstname',
                          lastname='Lastname')

        print_row(_d, _col_fields, headcol, bold=True)

        for i in self.entry_list:
            _d = get_form_row(subs_id=i[HTTP_CONT_FIELDS['SubscriberId']],
                              firstname=i[HTTP_CONT_FIELDS['FirstName']],
                              lastname=i[HTTP_CONT_FIELDS['LastName']])

            print_row(_d, _col_fields, rowcol, bold=False)

    """Print an standard message after successfully action"""
    def print_action(self, entry, action='', headcol='white', rowcol='yellow'):
        _col_fields = '{subs_id} {firstname}{lastname} {action}'

        if self.print_header:
            _d = get_form_row(subs_id='Subs.Id',
                              firstname='Firstname',
                              lastname='Lastname',
                              action='Action')

            print_row(_d, _col_fields, headcol, bold=True)

        _d = get_form_row(subs_id=entry[HTTP_CONT_FIELDS['SubscriberId']],
                          firstname=entry[HTTP_CONT_FIELDS['FirstName']],
                          lastname=entry[HTTP_CONT_FIELDS['LastName']],
                          action=action)

        print_row(_d, _col_fields, rowcol, bold=False)

    @_ExceptionCatch()
    @_CheckPrompt(pmsg='You\'re about to delete all these subscriber entries:',
                  emsg='Subscriber-Entry deleting procedure was canceled')
    def delete(self):
        _hrl_subs_sap = SubscriberSAP(hostport=self.hostport, scheme=self.scheme)
        _hrl_circ_sap = CircuitSAP(hostport=self.hostport, scheme=self.scheme)

        self.print_header = True

        for i in self.entry_list:
            _action_msg = 'Deleted'
            _row_col = 'yellow'
            try:
                _hrl_subs_sap.get(id=i[HTTP_CONT_FIELDS['SubscriberId']])

                if HTTP_CONT_FIELDS['Circuits'] in _hrl_subs_sap.received_sdu[0]:
                    for j in _hrl_subs_sap.received_sdu[0][HTTP_CONT_FIELDS['Circuits']]:
                        _hrl_circ_sap.delete(id=j[HTTP_CONT_FIELDS['CircuitId']],
                                             sid=i[HTTP_CONT_FIELDS['SubscriberId']])
                else:
                    _logger.debug('The entry id={} has no circuits'.format(i[HTTP_CONT_FIELDS['SubscriberId']]))

                _hrl_subs_sap.delete(id=i[HTTP_CONT_FIELDS['SubscriberId']])

            except SimplexHTTPError as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
                _row_col = 'red'
            except SimplexException as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
                _row_col = 'red'

            self.print_action(entry=i, action=_action_msg, rowcol=_row_col)
            self.print_header = False

        del _hrl_subs_sap, _hrl_circ_sap
        return

    @_ExceptionCatch()
    def create(self):
        _hrl_subs_sap = SubscriberSAP(hostport=self.hostport, scheme=self.scheme)
        _action_msg = 'Created'
        _row_col = 'yellow'

        try:
            _hrl_subs_sap.sent_sdu = {
                HTTP_CONT_FIELDS['FirstName']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['DataSubsFirstName']],
                HTTP_CONT_FIELDS['LastName']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['DataSubsLastName']]
            }
            _hrl_subs_sap.post()

            if self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['AppendCirc']]:
                _subs_id = _hrl_subs_sap.received_sdu[0][HTTP_CONT_FIELDS['SubscriberId']]
                _hrl_circ_sap = CircuitSAP(hostport=self.hostport, scheme=self.scheme)
                _hrl_circ_sap.sent_sdu = {
                    HTTP_CONT_FIELDS['Managed']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['AppendManaged']],
                    HTTP_CONT_FIELDS['IPv4']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['Ipv4']]
                }
                _hrl_circ_sap.post(sid=_subs_id)
                del _hrl_circ_sap

        except SimplexHTTPError as e:
            _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
            _row_col = 'red'
        except SimplexException as e:
            _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
            _row_col = 'red'

        self.print_action(entry=_hrl_subs_sap.received_sdu[0], action=_action_msg, rowcol=_row_col)

        del _hrl_subs_sap


class CircuitTransaction(SimplexTransaction):
    class _ExceptionCatch(SimplexTransaction.ExceptionCatch):
        pass

    class _CheckPrompt(SimplexTransaction.CheckPrompt):
        pass

    def __init__(self, hostport=None, scheme=DEF_SCHEME, quite=False, options=None):
        super().__init__(hostport=hostport, scheme=scheme, quite=quite, options=options)

    """Print a complete table of entries from a previous built entry dictionary"""
    @_ExceptionCatch()
    def print_table(self, headcol='white', rowcol='green'):
        _d = get_form_row(circ_id='Circ.Id',
                          status='Disabled',
                          username='Username',
                          passwd='Password',
                          nas_pid='NAS Port Id',
                          ipv4_addr='Framed PPP Addr',
                          ipv6_dp='Del.IPv6 Prefix',
                          firstname='Firstname',
                          lastname='Lastname',
                          subs_id='Subs.Id',
                          manag='Managed',
                          transp_id='Trans.Id',
                          srv_name='Serv.Name')

        _col_fields = '{circ_id} {username}{nas_pid}{ipv4_addr}{ipv6_dp} {status} {manag} ' \
                      '{subs_id} {firstname} {lastname} {transp_id} {srv_name}'

        if self.options[REF_SDU_KEYS['ShowSecret']] is True:
            _col_fields = '{circ_id} {username}{passwd}{nas_pid}{ipv4_addr}{subs_id} {firstname} ' \
                          '{lastname} {transp_id} {srv_name}'

        print_row(_d, _col_fields, headcol, bold=True)

        for i in self.entry_list:
            _transp_id = 'None'
            _srv_name = 'None'

            if i[HTTP_CONT_FIELDS['CircuitTranspId']] is not None:
                _transp_id = i[HTTP_CONT_FIELDS['CircuitTranspId']]
                _srv_name = i[HTTP_CONT_FIELDS['CircuitTransp']][
                    HTTP_CONT_FIELDS['TransportData']][
                    HTTP_CONT_FIELDS['TransportName']]

            _d = get_form_row(circ_id=i[HTTP_CONT_FIELDS['CircuitId']],
                              status=i[HTTP_CONT_FIELDS['Status']],
                              username=i[HTTP_CONT_FIELDS['UserName']],
                              passwd=i[HTTP_CONT_FIELDS['Password']],
                              nas_pid=i[HTTP_CONT_FIELDS['NasPortId']],
                              ipv4_addr=i[HTTP_CONT_FIELDS['IPv4']],
                              ipv6_dp=i[HTTP_CONT_FIELDS['IPv6']],
                              firstname=i[HTTP_CONT_FIELDS['Subscription']][HTTP_CONT_FIELDS['FirstName']],
                              lastname=i[HTTP_CONT_FIELDS['Subscription']][HTTP_CONT_FIELDS['LastName']],
                              subs_id=i[HTTP_CONT_FIELDS['Subscription']][HTTP_CONT_FIELDS['SubscriberId']],
                              manag=i[HTTP_CONT_FIELDS['Managed']],
                              transp_id=_transp_id,
                              srv_name=_srv_name)

            print_row(_d, _col_fields,
                      rowcol if i[HTTP_CONT_FIELDS['Status']] is False else 'red', bold=False)

    """Print an standard message after successfully action"""
    def print_action(self, entry, action='', headcol='white', rowcol='yellow'):

        _col_fields = '{circ_id} {username} {subs_id} {firstname} {lastname}{status} {action}'

        if self.print_header:
            _d = get_form_row(circ_id='Circ.Id',
                              status='Disabled',
                              username='Username',
                              firstname='Firstname',
                              lastname='Lastname',
                              subs_id='Subs.Id',
                              action='Action')

            print_row(_d, _col_fields, headcol, bold=True)

        _d = get_form_row(circ_id=entry[HTTP_CONT_FIELDS['CircuitId']],
                          status=entry[HTTP_CONT_FIELDS['Status']],
                          username=entry[HTTP_CONT_FIELDS['UserName']],
                          firstname=entry[HTTP_CONT_FIELDS['Subscription']][HTTP_CONT_FIELDS['FirstName']],
                          lastname=entry[HTTP_CONT_FIELDS['Subscription']][HTTP_CONT_FIELDS['LastName']],
                          subs_id=entry[HTTP_CONT_FIELDS['Subscription']][HTTP_CONT_FIELDS['SubscriberId']],
                          action=action)

        print_row(_d, _col_fields, rowcol, bold=False)

    """Delete a list of class-type entries"""
    @_ExceptionCatch()
    @_CheckPrompt(pmsg='You\'re about to apply delete procedure to all these circuit entries:',
                  emsg='Circuit-Entry deleting procedure was canceled')
    def delete(self):
        _hrl_sap = CircuitSAP(hostport=self.hostport, scheme=self.scheme)

        self.print_header = True

        for i in self.entry_list:
            _action_msg = '{} Deleted'.format('Transport' if self.options[REQ_KEYS['REQ_DELTR_KEY']] else 'Circuit')
            _row_col = 'yellow'
            try:
                _hrl_sap.delete(id=i[HTTP_CONT_FIELDS['CircuitId']],
                                sid=i[HTTP_CONT_FIELDS['SubscriptionId']],
                                deltr=self.options[REQ_KEYS['REQ_DELTR_KEY']])
            except SimplexHTTPError as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
                _row_col = 'red'
            except SimplexException as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
                _row_col = 'red'

            self.print_action(entry=i, action=_action_msg, rowcol=_row_col)
            self.print_header = False

        del _hrl_sap
        return

    """Create a single class-type entry"""
    @_ExceptionCatch()
    def create(self):
        _hrl_sap = CircuitSAP(hostport=self.hostport, scheme=self.scheme)
        _action_msg = 'Created'
        _row_col = 'yellow'

        try:
            _hrl_sap.sent_sdu = {
                HTTP_CONT_FIELDS['Managed']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['AppendManaged']],
                HTTP_CONT_FIELDS['IPv4']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['Ipv4']],
                HTTP_CONT_FIELDS['IPv6']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['Ipv6']],
                HTTP_CONT_FIELDS['UserName']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['Username']],
                HTTP_CONT_FIELDS['Password']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['Password']]
            }
            _hrl_sap.post(sid=self.options[REF_SDU_KEYS['SubsInstance']])

        except SimplexHTTPError as e:
            _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
            _row_col = 'red'
        except SimplexException as e:
            _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
            _row_col = 'red'

        self._get_entry_list()  # Force to create a complete entry list because API does not report enough data
        self.print_header = True

        for i in self.entry_list:
            if i[HTTP_CONT_FIELDS['CircuitId']] == _hrl_sap.received_sdu[0][HTTP_CONT_FIELDS['CircuitId']]:
                self.print_action(entry=i, action=_action_msg, rowcol=_row_col)
                self.print_header = False

        del _hrl_sap

    """Set a list of class-type entry"""
    @_ExceptionCatch()
    @_CheckPrompt(pmsg='You\'re about to set over these circuit entries:',
                  emsg='Circuit-Entry setting procedure was canceled')
    def set(self):
        _hrl_sap = CircuitSAP(hostport=self.hostport, scheme=self.scheme)

        self.print_header = True

        for i in self.entry_list:
            _action_msg = '{}'.format(self.options[REQ_KEYS['REQ_EPCOM_KEY']]
                                      if not self.options[REQ_KEYS['REQ_TRPID_KEY']] else 'Attach transport')
            _row_col = 'yellow'
            try:
                _hrl_sap.patch(id=i[HTTP_CONT_FIELDS['CircuitId']],
                               sid=i[HTTP_CONT_FIELDS['SubscriptionId']],
                               cmd=self.options[REQ_KEYS['REQ_EPCOM_KEY']],
                               tid=self.options[REQ_KEYS['REQ_TRPID_KEY']])
            except SimplexHTTPError as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
                _row_col = 'red'
            except SimplexException as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
                _row_col = 'red'

            self.print_action(entry=i, action=_action_msg, rowcol=_row_col)
            self.print_header = False

        del _hrl_sap
        return


class CarrierTransaction(SimplexTransaction):
    class _ExceptionCatch(SimplexTransaction.ExceptionCatch):
        pass

    class _CheckPrompt(SimplexTransaction.CheckPrompt):
        pass

    def __init__(self, hostport=None, scheme=DEF_SCHEME, quite=False, options=None):
        super().__init__(hostport=hostport, scheme=scheme, quite=quite, options=options)

    """Print a complete table of entries from a previous built entry dictionary"""
    @_ExceptionCatch()
    def print_table(self, headcol='white', rowcol='cyan'):
        _d = get_form_row(carrier_id='Carr.Id',
                          carrier_name='Carrier Name')

        _col_fields = '{carrier_id} {carrier_name}'

        print_row(_d, _col_fields, headcol, bold=True)

        for i in self.entry_list:
            _d = get_form_row(carrier_id=i[HTTP_CONT_FIELDS['CommonId']],
                              carrier_name=i[HTTP_CONT_FIELDS['CarrierName']])

            print_row(_d, _col_fields, rowcol, bold=False)

    """Print an standard message after successfully action"""
    def print_action(self, entry, action='', headcol='white', rowcol='yellow'):
        _col_fields = '{carrier_id} {carrier_name} {action}'

        if self.print_header:
            _d = get_form_row(carrier_id='Carr.Id',
                              carrier_name='Carrier Name',
                              action='Action')

            print_row(_d, _col_fields, headcol, bold=True)

        _d = get_form_row(carrier_id=entry[HTTP_CONT_FIELDS['CommonId']],
                          carrier_name=entry[HTTP_CONT_FIELDS['CarrierName']],
                          action=action)

        print_row(_d, _col_fields, rowcol, bold=False)

    """Delete a list of class-type entries"""
    @_ExceptionCatch()
    @_CheckPrompt(pmsg='You\'re about to delete all these carrier entries:',
                  emsg='Carrier-Entry deleting procedure was canceled')
    def delete(self):
        _hrl_sap = CarrierSAP(hostport=self.hostport, scheme=self.scheme)
        _action_msg = 'Deleted'
        _row_col = 'yellow'

        self.print_header = True

        for i in self.entry_list:
            try:
                _hrl_sap.delete(id=i[HTTP_CONT_FIELDS['CommonId']])
                
            except SimplexHTTPError as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
                _row_col = 'red'
            except SimplexException as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
                _row_col = 'red'

            self.print_action(entry=i, action=_action_msg, rowcol=_row_col)
            self.print_header = False

        del _hrl_sap
        return

    """Create a single class-type entry"""
    @_ExceptionCatch()
    def create(self):
        _hrl_sap = CarrierSAP(hostport=self.hostport, scheme=self.scheme)
        _action_msg = 'Created'
        _row_col = 'yellow'

        try:
            _hrl_sap.sent_sdu = {
                HTTP_CONT_FIELDS['CarrierName']: self.options[REF_SDU_KEYS['PayLoad']][REF_SDU_KEYS['CarrierName']]
            }
            _hrl_sap.post()

        except SimplexHTTPError as e:
            _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
            _row_col = 'red'
        except SimplexException as e:
            _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
            _row_col = 'red'

        # Problems arise here if some error takes place in server and it does not send any expected sdu
        # We should review the API design
        self.print_action(entry=_hrl_sap.received_sdu[0], action=_action_msg, rowcol=_row_col)

        del _hrl_sap


class TransportTransaction(SimplexTransaction):
    class _ExceptionCatch(SimplexTransaction.ExceptionCatch):
        pass

    class _CheckPrompt(SimplexTransaction.CheckPrompt):
        pass

    """Validate the template file and initialize related dictionary"""
    class _ValidateDataTemplate:
        def __call__(self, *call_ref):
            def wrapper(obj, **kwargs):
                _tm = ''
                _tr = ''

                for k, v in obj.options.items():
                    if k == REQ_KEYS['REQ_TEMPL_KEY'] and v:
                        _tm = str(v)
                    elif k == REQ_KEYS['REQ_TRREF_KEY'] and v:
                        _tr = str(v)
                    else:
                        _logger.debug('Key \'{}={}\' has empty value or does not match with any predefined key'.
                                      format(str(k), str(v)))

                _templ_list = get_transp_settings(_tm)

                if not _templ_list:
                    raise SimplexException('Data template list is empty', 'Template Validation')

                for i in _templ_list:
                    try:
                        if i[TEMPL_SREF_KEY] == TEMPL_TRBE1_VAL:
                            jsonschema.validate(instance=i, schema=TRBE1_SCHEME)
                        elif i[TEMPL_SREF_KEY] == TEMPL_TRBE2_VAL:
                            jsonschema.validate(instance=i, schema=TRBE2_SCHEME)
                        else:
                            raise SimplexException('Unrecognized key {}={} in file {}'.format(str(TEMPL_SREF_KEY),
                                                                                              str(i[TEMPL_SREF_KEY]),
                                                                                              str(_tm)),
                                                   'Template Validation')

                    except jsonschema.exceptions.ValidationError:
                        raise

                    if i[TEMPL_TREF_KEY] == _tr:
                        obj.built_data = i

                if not obj.built_data:
                    raise SimplexException('Value \'{}\' not found in referred template'.format(str(_tr)),
                                           'Template validation')

                return call_ref[0](obj, **kwargs)
            return wrapper

    def __init__(self, hostport=None, scheme=DEF_SCHEME, quite=False, options=None):
        super().__init__(hostport=hostport, scheme=scheme, quite=quite, options=options)

    """Print a complete table of entries from a previous built entry dictionary"""
    @_ExceptionCatch()
    def print_table(self, headcol='white', rowcol='cyan'):
        _col_fields = '{transp_id} {carrier_id} {srv_name} {vlan_set} {transp_addons} {srv_location}'

        _d = get_form_row(carrier_id='Carr.Id',
                          transp_id='Trans.Id',
                          srv_name='Name',
                          vlan_set='Vlan Set',
                          transp_addons='Endpoint Data',
                          srv_location='Location')

        print_row(_d, _col_fields, headcol, bold=True)

        for i in self.entry_list:
            _vlan_set = 'S={:>4}:{:>1} C={:>4}:{:>1} R={:>4}:{:>1}'.format(
                str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportSvlan']]
                    [HTTP_CONT_FIELDS['TransportVid']]),
                str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportSvlan']]
                    [HTTP_CONT_FIELDS['TransportPcp']]),
                str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportCvlan']]
                    [HTTP_CONT_FIELDS['TransportVid']]),
                str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportCvlan']]
                    [HTTP_CONT_FIELDS['TransportPcp']]),
                str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportRvlan']]
                    [HTTP_CONT_FIELDS['TransportVid']]),
                str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportRvlan']]
                    [HTTP_CONT_FIELDS['TransportPcp']]))

            if i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportSref']] \
                    == TEMPL_TRBE1_VAL:
                _addons = 'snAuth= {}, passAuth= {}'.format(
                    str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportSnAuth']]),
                    str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportPassAuth']]))
            elif i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportSref']] \
                    == TEMPL_TRBE2_VAL:
                _addons = 'eoIpAddr= {}, eoIpTunId= {}'.format(
                    str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportEoipAddr']]),
                    str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportEoipTunId']]))
            else:
                _logger.debug('Template reference value \'{}\' unrecognized'.format(
                    str(i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportSref']])))
                continue

            _d = get_form_row(carrier_id=i[HTTP_CONT_FIELDS['CarrierId']],
                              transp_id=i[HTTP_CONT_FIELDS['TransportId']],
                              srv_name=i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportName']],
                              vlan_set=_vlan_set,
                              transp_addons=_addons,
                              srv_location=i[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportLocation']],)

            print_row(_d, _col_fields, rowcol, bold=False)

    """Print an standard message after successfully action"""
    def print_action(self, entry, action='', headcol='white', rowcol='yellow'):
        _col_fields = '{carrier_id} {srv_name} {srv_location} {action}'

        if self.print_header:
            _d = get_form_row(carrier_id='Carr.Id',
                              srv_name='Name',
                              srv_location='Location',
                              action='Action')

            print_row(_d, _col_fields, headcol, bold=True)

        _d = get_form_row(carrier_id=entry[HTTP_CONT_FIELDS['CarrierId']],
                          srv_name=entry[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportName']],
                          srv_location=entry[HTTP_CONT_FIELDS['TransportData']][HTTP_CONT_FIELDS['TransportLocation']],
                          action=action)

        print_row(_d, _col_fields, rowcol, bold=False)

    """Create a single class-type entry"""
    @_ExceptionCatch()
    @_ValidateDataTemplate()
    def create(self):
        self.over_templ_sref = (TEMPL_SREF_KEY, self.built_data[TEMPL_SREF_KEY])
        self._build_from_dict(curr_dict=self.built_data, force_prompt=self.options[REF_SDU_KEYS['ForcePrompt']])

        if self.built_data[HTTP_CONT_FIELDS['TransportSref']] == TEMPL_TRBE1_VAL:
            jsonschema.validate(instance=self.built_data, schema=TRBE1_SCHEME)
        elif self.built_data[HTTP_CONT_FIELDS['TransportSref']] == TEMPL_TRBE2_VAL:
            jsonschema.validate(instance=self.built_data, schema=TRBE2_SCHEME)

        if click.confirm(click.style('Do you want to create this entry?', fg='white')):
            _hrl_sap = TransportSAP(hostport=self.hostport, scheme=self.scheme)
            _action_msg = 'Created'
            _row_col = 'yellow'

            try:
                _hrl_sap.sent_sdu = {
                    HTTP_CONT_FIELDS['TransportData']: self.built_data,
                    HTTP_CONT_FIELDS['CarrierId']: self.options[REF_SDU_KEYS['AttachToCarrier']]
                }
                _hrl_sap.post()

            except SimplexHTTPError as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
                _row_col = 'red'
            except SimplexException as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
                _row_col = 'red'

            # Problems arise here if some error takes place in server and it does not send any expected sdu
            # We should review the API design
            self.print_action(entry=_hrl_sap.sent_sdu, action=_action_msg, rowcol=_row_col)

            del _hrl_sap
            return

        raise SimplexException('Transport-Entry creation procedure was canceled', '')

    """Delete a list of class-type entries"""
    @_ExceptionCatch()
    @_CheckPrompt(pmsg='You\'re about to delete all these transport entries:',
                  emsg='Transport-Entry deleting procedure was canceled')
    def delete(self):
        _hrl_sap = TransportSAP(hostport=self.hostport, scheme=self.scheme)
        _action_msg = 'Deleted'
        _row_col = 'yellow'

        self.print_header = True

        for i in self.entry_list:
            try:
                _hrl_sap.delete(id=i[HTTP_CONT_FIELDS['TransportId']])

            except SimplexHTTPError as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_http_err'].format(str(e.status_code), e.message)
                _row_col = 'red'
            except SimplexException as e:
                _action_msg = PRINT_FORMAT_FIELDS['action_err'].format(e.message)
                _row_col = 'red'

            self.print_action(entry=i, action=_action_msg, rowcol=_row_col)
            self.print_header = False

        del _hrl_sap
        return
