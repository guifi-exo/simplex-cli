import requests
import json

from simplex_cli.utils import \
    get_form_row, \
    print_row, \
    get_row_message
from simplex_cli.constants import REQ_KEYS


class SimplexException(Exception):
    def __init__(self, msg, ename):
        super().__init__(msg)
        self.message = msg
        self.except_name = ename

        if not ename:
            self.except_name = self.__class__.__name__

    def __str__(self):
        return self.get_error_resp()

    """Build formatted exception message"""
    def get_error_resp(self):
        _d = get_form_row(excep_name='Exception Name',
                          msg='Message')

        _header = get_row_message(_d, '{excep_name}'
                                      '{msg}')

        _d = get_form_row(excep_name=self.except_name,
                          msg=self.message)

        _row = get_row_message(_d, '{excep_name}'
                                   '{msg}')

        return _header + '\n' + _row

    """Print formatted exception message"""
    def print_error_resp(self, headcol='white', rowcolor='red'):
        if self.except_name == 'Abort':
            return

        _d = get_form_row(excep_name='Exception Name',
                          msg='Message')

        print_row(_d, '{excep_name}'
                      '{msg}', headcol, bold=True)

        _d = get_form_row(excep_name=self.except_name,
                          msg=self.message)

        print_row(_d, '{excep_name}'
                      '{msg}', rowcolor, bold=False)


class SimplexHTTPError(SimplexException):
    def __init__(self, msg, ename, statuscode, resp):
        super().__init__(msg, ename)
        self.status_code = statuscode
        self.response = requests.Response()
        self.body = dict()
        self.method = 'Unkwon'

        if resp is not None:
            try:
                self.response = resp
                self.status_code = self.response.status_code
                self.method = self.response.request.method
                self.body = self.response.json()

            except json.decoder.JSONDecodeError:
                pass
            except requests.exceptions:
                pass

    def print_error_resp(self, **kwargs):
        _sresp = False
        _method = self.method
        _headcol = 'white'
        _rowcol = 'white'

        for k, v in kwargs.items():
            if k == REQ_KEYS['REQ_SRESP_KEY'] and v:
                _sresp = bool(v)
            elif k == REQ_KEYS['REQ_HDCOL_KEY'] and v:
                _headcol = str(v)
            elif k == REQ_KEYS['REQ_RWCOL_KEY'] and v:
                _rowcol = str(v)

        # Print table header
        _d = get_form_row(excep_name='Exception name',
                          method='Method',
                          status_code='Code',
                          msg='Error message')

        print_row(_d, '{excep_name}'
                      '{method}'
                      '{status_code}'
                      '{msg}', _headcol, bold=True)

        # Print table row data
        _d = get_form_row(method=_method,
                          excep_name=self.except_name,
                          status_code=self.status_code,
                          msg=self.message)

        print_row(_d, '{excep_name}'
                      '{method}'
                      '{status_code}'
                      '{msg}', _rowcol, bold=False)

        if _sresp:
            self.print_resp_json()

    def print_resp_json(self, headcol='yellow', rowcolor='yellow'):
        _d = get_form_row(msg='JSON Retrieved Response')
        print_row(_d, '{msg}', headcol, bold=True)
        _d = get_form_row(json=json.dumps(self.body, indent=2, sort_keys=True))
        print_row(_d, '{json}', rowcolor, bold=False)
