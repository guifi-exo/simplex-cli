import logging
import sys
import click
import re

from simplex_cli.constants import \
    PRINT_FORMAT_FIELDS, \
    STRING_DEFVAL_REGEXP, \
    HTTP_CONT_FIELDS

from simplex_cli.schemes import \
    REF_SDU_VALUES

_logger = logging.getLogger(__name__)


def conv_to_string(var):
    if type(var) is str:
        return var
    elif type(var) is int:
        return str(var)
    elif type(var) is bool:
        return '{}'.format('True' if var else 'False')
    elif var is None:
        return ''
    else:
        _logger.warning('Can not convert from {} to string'.format(str(type(var))))


def get_keymatch_value(dct, key):
    for k, v in sorted(dct.items()):
        if isinstance(v, dict):
            yield from get_keymatch_value(v, key)
        elif isinstance(v, list):
            _logger.debug('A list was found in the recursive search. This item is bypassed.')
        elif k == key:
            yield v


def match_field_value(dct=dict, match_field='', match_expr=''):
    if not match_expr:
        return True

    for v in get_keymatch_value(dct, match_field):
        v = conv_to_string(v)
        _re_match = re.match(r'{}'.format(match_expr), v)
        if _re_match:
            return True
        else:
            return False


def scheme_string_prompt(item, dct, curr_key, parent_key, force_prompt=False, overr_key=()):
    # If override key-value pair is found, do not prompt
    if curr_key in overr_key:
        dct[curr_key] = overr_key[1]
        return

    if not item:
        dct[curr_key] = click.prompt(click.style(PRINT_FORMAT_FIELDS['data_prompt'].format(str(parent_key),
                                                                                           str(curr_key)),
                                                 fg='white'),
                                     type=str)
    else:
        def_value = re.search(STRING_DEFVAL_REGEXP, item)

        # Only prompt if parsed item is empty. If so, prompt with item default value
        if def_value is None:
            if force_prompt is True:
                dct[curr_key] = click.prompt(click.style(PRINT_FORMAT_FIELDS['data_prompt'].format(str(parent_key),
                                                                                                   str(curr_key)),
                                                         fg='white'),
                                             type=str, default=item)
            else:
                dct[curr_key] = item
        else:
            dct[curr_key] = click.prompt(click.style(PRINT_FORMAT_FIELDS['data_prompt'].format(str(parent_key),
                                                                                               str(curr_key)),
                                                     fg='white'),
                                         type=str, default=def_value.group(1))


def scheme_int_prompt(item, dct, curr_key, parent_key, force_prompt=False, overr_key=()):
    # If override key-value pair is found, do not prompt
    if curr_key in overr_key:
        dct[curr_key] = overr_key[1]
        return

    # Only prompt if item is positive integer. If so, prompt with item default value
    if item >= 0:
        if force_prompt is True:
            dct[curr_key] = click.prompt(click.style(PRINT_FORMAT_FIELDS['data_prompt'].format(str(parent_key),
                                                                                               str(curr_key)),
                                                     fg='white'),
                                         type=int, default=item)
    else:
        dct[curr_key] = click.prompt(click.style(PRINT_FORMAT_FIELDS['data_prompt'].format(str(parent_key),
                                                                                           str(curr_key)),
                                                 fg='white'),
                                     type=int)


def get_row_message(row_dct, fld_order=''):
    return fld_order.format(**row_dct)


"""
def print_row(self, row_dct, fld_order='', fg='', bold=False, reverse=False):
    click.secho(fld_order.format(**row_dct), fg=fg, bold=bold, reverse=reverse)
"""


def print_row(row_dct, fld_order, fg='', bold=False, reverse=False):
    click.secho(get_row_message(row_dct, fld_order), fg=fg, bold=bold, reverse=reverse)


def setup_logging(loglevel):
    message_format = '[%(asctime)s] %(levelname)s {modu_name}{func_name}(){line_numb}: %(message)s'

    logformat = message_format.format(func_name=': %(funcName)s' if loglevel == 'DEBUG' else '',
                                      modu_name=': %(module)s' if loglevel == 'DEBUG' else '',
                                      line_numb=': %(lineno)d' if loglevel == 'DEBUG' else '')

    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def get_form_row(**kwargs):
    _d = dict()

    for k in sorted(kwargs.keys()):
        _is_intfloat = False
        if k in PRINT_FORMAT_FIELDS:
            if type(kwargs[k]) in (float, int):
                _is_intfloat = True

            _d[k] = PRINT_FORMAT_FIELDS[k].format(kwargs[k] if _is_intfloat else str(kwargs[k]))
        else:
            _logger.debug('The key \'{}\' is not present in predefined format fields'.format(str(k)))

    return _d


def get_circuit_cmds(ctx, args, incomplete):
    cmds = [REF_SDU_VALUES['DataInstEnable'],
            REF_SDU_VALUES['DataInstDisable'],
            REF_SDU_VALUES['DataInstDisconn']]
    return [c for c in cmds if incomplete in c]


def get_subscriber_mfields(ctx, args, incomplete):
    mfields = [HTTP_CONT_FIELDS['FirstName'],
               HTTP_CONT_FIELDS['LastName']]
    return [c for c in mfields if incomplete in c]


def get_circuit_mfields(ctx, args, incomplete):
    mfields = [HTTP_CONT_FIELDS['UserName'],
               HTTP_CONT_FIELDS['IPv4'],
               HTTP_CONT_FIELDS['IPv6'],
               HTTP_CONT_FIELDS['NasPortId'],
               HTTP_CONT_FIELDS['Status'],
               HTTP_CONT_FIELDS['Managed'],
               HTTP_CONT_FIELDS['FirstName'],
               HTTP_CONT_FIELDS['LastName']]
    return [c for c in mfields if incomplete in c]


def get_transport_mfields(ctx, args, incomplete):
    mfields = [HTTP_CONT_FIELDS['TransportName'],
               HTTP_CONT_FIELDS['TransportLocation']]
    return [c for c in mfields if incomplete in c]


def get_carrier_mfields(ctx, args, incomplete):
    mfields = [HTTP_CONT_FIELDS['CarrierName']]
    return [c for c in mfields if incomplete in c]

# def print_connection_tracking(scheme, auth):
#     click.secho('Building request to SAPI server authority {scheme}://{auth}'.format(scheme=scheme,
#                                                                                      auth=auth), fg='white')


# def print_request_tracking(msg):
#     click.secho('{}'.format(msg), fg='white')


# def print_war_games():
#     click.secho('List Games\n', fg='white')
#     click.secho('FALKEN\'S MAZE\nBLACKJACK\nGIN RUMMY\nHEARTS\nBRIDGE\nCHECKERS\nCHESS\n'
#                'POKER\nFIGHTER COMBAT\nGUERRILLA ENGAGEMENT\nDESERT WARFARE\n'
#                'AIR-TO-GROUND ACTIONS\nTHEATERWIDE TACTICAL WARFARE\n'
#                'THEATERWIDE BIOTOXIC AND CHEMICAL WARFARE\n\nGLOBAL THERMONUCLEAR WAR', fg='white')
