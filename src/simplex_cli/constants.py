from simplex_cli import \
    __version__, \
    __user_agent__

"""Regular expressions"""
SOCKET_REGEXP = r'(\[[:a-fA-F0-9]+\]|(?:\d{1,3}\.){3}\d{1,3}|[-a-zA-Z0-9.]+)(?::(\d+))?'
SNAUTH_REGEXP = "^(00f[0-9]{13})?$"
PSAUTH_REGEXP = "^(f[0-9]{13})?$"
INHERIT_EXCEP_REGEXP = r'<.*?>: '
STRING_DEFVAL_DELIM = '%'
STRING_DEFVAL_REGEXP = r'\{}([^(^)]+)\{}'.format(STRING_DEFVAL_DELIM, STRING_DEFVAL_DELIM)

"""General constants"""
SCHEME_OPTS = ('http', 'https')
DEF_SCHEME = SCHEME_OPTS[0]
CONN_TIMEOUT = 10

REQ_KEYS = {
    'REQ_FIELD_KEY': 'field',
    'REQ_VALUE_KEY': 'value',
    'REQ_IDENT_KEY': 'id',
    'REQ_HDATA_KEY': 'data',
    'REQ_EPCOM_KEY': 'cmd',
    'REQ_TRPID_KEY': 'tid',
    'REQ_QNPID_KEY': 'npid',
    'REQ_IP4ID_KEY': 'ipid',
    'REQ_SBEID_KEY': 'sid',
    'REQ_SRESP_KEY': 'sresp',
    'REQ_ACTIO_KEY': 'action',
    'REQ_HDCOL_KEY': 'headcol',
    'REQ_RWCOL_KEY': 'rowcol',
    'REQ_SSTAT_KEY': 'state',
    'REQ_PDISC_KEY': 'disc',
    'REQ_DELTR_KEY': 'deltr',  # Deleting transport entry attached to a circuit
    'REQ_MFIEL_KEY': 'match_field',  # This code should be removed when the API implement RE searches
    'REQ_MVALU_KEY': 'match_expr',  # This code should be removed when the API implement RE searches
    'REQ_TEMPL_KEY': 'template',
    'REQ_TRREF_KEY': 'tref',  # Transport reference from template list of transpor data
    'REQ_CHPRO_KEY': 'pmsg',
    'REQ_CHEXC_KEY': 'emsg'
}

HTTP_HEAD_FIELDS = {
    'UserAgent': 'User-Agent',
    'Accept': 'Accept',
    'ContentType': 'Content-Type'
}

HTTP_HEAD_VALUES = {
    'ApplicationJSON': 'application/json',
    'SpxUserAgent': '{}/{}'.format(__user_agent__, __version__),
    'AllMedia': '*/*'
}

HTTP_CONT_FIELDS = {
    'FirstName': 'firstname',
    'LastName': 'lastname',
    'SubscriberId': 'id',
    'Status': 'isDisabled',
    'CircuitId': 'id',
    'Circuits': 'circuits',
    'CircuitTransp': 'transport',
    'CircuitTranspId': 'transportId',
    'UserName': 'username',
    'Password': 'password',
    'IPv4': 'ipv4',
    'IPv6': 'ipv6',
    'NasPortId': 'nasId',
    'SubscriptionDesc': 'subscription',
    'Greeting': 'greeting',
    'Date': 'date',
    'Path': 'url',
    'Host': 'host',
    'Subscription': 'subscription',
    'SubscriptionId': 'subscriptionId',
    'Message': 'msg',
    'Managed': 'hasManagement',
    'CarrierId': 'carrierId',
    'CarrierName': 'name',
    'CarrierData': 'data',
    'CarrierStruct': 'structure',
    'CommonId': 'id',
    'TransportData': 'data',
    'TransportId': 'id',
    'TransportSref': 'sref',
    'TransportName': 'name',
    'TransportLocation': 'location',
    'TransportSvlan': 's_vlan',
    'TransportCvlan': 'c_vlan',
    'TransportRvlan': 'r_vlan',
    'TransportVid': 'vid',
    'TransportPcp': 'pcp',
    'TransportEndDev': 'endpointDev',
    'TransportEoipAddr': 'eoipRemAddress',
    'TransportEoipTunId': 'eoipTunId',
    'TransportSnAuth': 'snAuth',
    'TransportPassAuth': 'passAuth'

}

HTTP_ERROR_FIELDS = {
    'StatusCode': 'statusCode',
    'Message': 'message',
    'Error': 'error',
    'Name': 'name'
}

"""Fields used to build formatted strings in order to be printed on terminal"""
PRINT_FORMAT_FIELDS = {
    'subs_id': '{:>8}',
    'status': '{:>8}',
    'username': '{:<20}',
    'passwd': '{:<35.35}',
    'ipv4_addr': '{:>16.16}',
    'ipv6_dp': '{:>30.30}',
    'nas_pid': '{:<12}',
    'firstname': '{:<15.15}',
    'lastname': '{:<15.15}',
    'date': '{:<30.30}',
    'greeting': '{:<25.25}',
    'path': '{:<80.80}',
    'url': '{:<120.120}',
    'circ_id': '{:>8}',
    'msg': '{:<100.100}',
    'status_code': '{:<8}',
    'json': '{}',
    'method': '{:<10.10}',
    'action': '{:<50.50}',
    'action_err': 'Error= {}',
    'action_http_err': 'Code= {} >> {}',
    'excep_name': '{:<30.30}',
    'manag': '{:>8}',
    'data_prompt': '> {}/{}',
    'carrier_id': '{:>10}',
    'carrier_name': '{:<20.20}',
    'transp_id': '{:>8}',
    'srv_name': '{:<15.15}',
    'srv_location': '{:<40.40}',
    'sref': '{:<12}',
    'vlan_set': '{:<27.27}',
    'transp_addons': '{:<55.55}'
}
