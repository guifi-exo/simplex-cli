import configparser
# import pkg_resources
import json
import os

# from simplex_cli import dist_name
from simplex_cli.exceptions import SimplexException
from simplex_cli.utils import _logger


# Configuration file keys
CFG_FILENAME = os.getenv('HOME') + '/.spxc/spxc.cfg'
CFG_CONN_SEC_NAME = 'server'
CFG_CONN_DST_PORT = 'dst_port'
CFG_CONN_HOSTNAME = 'hostname'
CFG_CONN_SCHEME = 'scheme'

TRP_FILENAME = os.getenv('HOME') + '/.spxc/transp_default.json'


def get_config_settings(filename):
    try:
        if not filename:
            # config_file = pkg_resources.resource_filename(dist_name, CFG_FILENAME)
            config_file = CFG_FILENAME
        else:
            config_file = filename

        _logger.debug('Parsing config file: {}'.format(config_file))

        config_settings = configparser.ConfigParser()
        _ret = config_settings.read(config_file)

        if len(_ret) <= 0:
            raise Exception('File not found')

        return config_settings

    except Exception as e:
        raise SimplexException('Parsing settings from \'{}\' failed: {}'.format(filename, str(e)),
                               type(e).__name__)


def get_config_uri(filename):

    _config = get_config_settings(filename)

    try:
        _scheme = _config[CFG_CONN_SEC_NAME][CFG_CONN_SCHEME]
        _hostname = _config[CFG_CONN_SEC_NAME][CFG_CONN_HOSTNAME]
        _dstport = _config[CFG_CONN_SEC_NAME][CFG_CONN_DST_PORT]

        _logger.debug('Values parsed from config file \'{}\': CFG_CONN_SCHEME = {}, '
                      'CFG_CONN_HOSTNAME = {}, CFG_CONN_DST_PORT = {}'.format(filename,
                                                                              _scheme,
                                                                              _hostname,
                                                                              _dstport))

    except KeyError as f:
        raise SimplexException('Key {} not found in file \'{}\''.format(str(f), filename), type(f).__name__)

    return _scheme, _hostname, _dstport


def get_transp_settings(filename):
    try:
        if not filename:
            # transp_file = pkg_resources.resource_filename(dist_name, TRP_FILENAME)
            transp_file = TRP_FILENAME
        else:
            transp_file = filename

        _logger.debug('Parsing transport file: {}'.format(transp_file))

        with open(transp_file) as f:
            transp_settings = json.load(f)

        return transp_settings

    except Exception as e:
        raise SimplexException('Parsing settings from \'{}\' failed: {}'.format(filename, str(e)),
                               type(e).__name__)
