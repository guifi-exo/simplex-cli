import click

from simplex_cli import \
    __version__, \
    __cli_name__
from simplex_cli.constants import \
    DEF_SCHEME, \
    SCHEME_OPTS
from simplex_cli.settings import \
    get_config_uri
from simplex_cli.utils import \
    setup_logging, \
    _logger, \
    get_subscriber_mfields, \
    get_circuit_mfields, \
    get_transport_mfields, \
    get_carrier_mfields

from simplex_cli.exceptions import \
    SimplexException, \
    SimplexHTTPError

from simplex_cli.stl import \
    SubscriberTransaction, \
    CircuitTransaction, \
    CarrierTransaction, \
    TransportTransaction

from simplex_cli.schemes import \
    REF_SDU_VALUES


@click.group(invoke_without_command=False)
@click.option('-f', '--config-file', 'cfg', type=str, default='',
              help='Set specific config file')
@click.option('-a', '--authority', 'hostp', default='', type=str,
              help='Define the URI authority')
@click.option('-sh', '--scheme', 'sche', type=click.Choice(SCHEME_OPTS), default=DEF_SCHEME,
              help='Select the URI scheme in case you set authority')
@click.option('-j', '--show-json-resp', 'jrep', flag_value=True,
              help='Enable showing JSON messages if HTTP exceptions arise')
@click.option('--debug/--no-debug', default=False,
              help='Enable debug mode')
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx, debug, cfg, hostp, sche, jrep):

    ctx.ensure_object(dict)

    # if ctx.invoked_subcommand is None:
    #     click.secho('You have to specify one command. Set --help option to get more information.')
    #     cli.exit()

    ctx.obj['DEBUG'] = debug

    if ctx.obj['DEBUG']:
        setup_logging('DEBUG')
    else:
        setup_logging('INFO')

    if not hostp:
        try:
            uri_tokens = get_config_uri(cfg)
            hostp = '{}:{}'.format(uri_tokens[1], uri_tokens[2])
            sche = uri_tokens[0]
        except SimplexException as e:
            e.print_error_resp()
            ctx.abort()

    ctx.obj['SCHEME'] = sche
    ctx.obj['HOSTPORT'] = hostp
    ctx.obj['JSONRESP'] = jrep

    click.secho('Building request to SAPI server authority {scheme}://{auth}'.format(scheme=sche,
                                                                                     auth=hostp), fg='white')

    _logger.debug('Context variables: SCHEME = {}, HOSTPORT = {}, JSONRESP = {}, '.format(ctx.obj['SCHEME'],
                                                                                          ctx.obj['HOSTPORT'],
                                                                                          ctx.obj['JSONRESP']))


@cli.command(name='subscriber')
@click.pass_context
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_subscriber_mfields,
              help='Specify the field to be match')
@click.option('-me', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
def subscriber(ctx, ident, mfld, mexp):
    """Delete a list of subscriber entries"""

    click.secho('Deleting a list of subscribers')

    """The keys of options dict come from REQ_KEYS"""

    opt = {
            "type": REF_SDU_VALUES['SubscriberSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp  # regular expression
        }

    try:
        _stl = SubscriberTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.delete()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


@cli.command(name='circuit')
@click.pass_context
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_circuit_mfields,
              help='Specify the field to be match')
@click.option('-me', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
@click.option('-ss', '--show-secret', 'shsrc', flag_value=True,
              help='Show secret circuit-related data')
@click.option('-dt', '--delete-transp', 'deltr', flag_value=True,
              help='Delete just the attached transport entry')
def circuit(ctx, ident, mfld, mexp, shsrc, deltr):
    """Deleting procedure over a list of circuit entries"""

    click.secho('Deleting procedure over a list of circuits')

    """The keys of options dict come from REQ_KEYS"""

    opt = {
            "type": REF_SDU_VALUES['CircuitSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp,  # regular expression
            "show-secret": shsrc,
            "deltr": deltr
        }

    try:
        _stl = CircuitTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.delete()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


@cli.command(name='carrier')
@click.pass_context
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_carrier_mfields,
              help='Specify the field to be match')
@click.option('-me', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
def carrier(ctx, ident, mfld, mexp):
    """Delete a list of carrier entries"""

    click.secho('Deleting a list of carriers')

    """The keys of options dict come from REQ_KEYS"""

    opt = {
            "type": REF_SDU_VALUES['CarrierSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp  # regular expression
        }

    try:
        _stl = CarrierTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.delete()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


@cli.command(name='transport')
@click.pass_context
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_transport_mfields,
              help='Specify the field to be match')
@click.option('-me', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
def transport(ctx, ident, mfld, mexp):
    """Delete a list of transport entries"""

    click.secho('Deleting a list of transport entries')

    """The keys of options dict come from REQ_KEYS"""

    opt = {
            "type": REF_SDU_VALUES['TransportSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp  # regular expression
        }

    try:
        _stl = TransportTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.delete()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


if __name__ == '__main__':
    cli(obj={})
