import click

from simplex_cli import \
    __version__
from simplex_cli.constants import \
    DEF_SCHEME, \
    SCHEME_OPTS
from simplex_cli.settings import \
    get_config_uri
from simplex_cli.utils import \
    setup_logging, \
    _logger, \
    get_circuit_cmds, \
    get_circuit_mfields
from simplex_cli.exceptions import \
    SimplexException, \
    SimplexHTTPError
from simplex_cli.stl import \
    CircuitTransaction
from simplex_cli.schemes import \
    REF_SDU_VALUES


@click.group(invoke_without_command=False)
@click.option('-f', '--config-file', 'cfg', type=str, default='',
              help='Set specific config file')
@click.option('-a', '--authority', 'hostp', default='', type=str,
              help='Define the URI authority')
@click.option('-sh', '--scheme', 'sche', type=click.Choice(SCHEME_OPTS), default=DEF_SCHEME,
              help='Select the URI scheme in case you set authority')
@click.option('-j', '--show-json-resp', 'jrep', flag_value=True,
              help='Enable showing JSON messages if HTTP exceptions arise')
@click.option('--debug/--no-debug', default=False,
              help='Enable debug mode')
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx, debug, cfg, hostp, sche, jrep):

    ctx.ensure_object(dict)

    # if ctx.invoked_subcommand is None:
    #     click.secho('You have to specify one command. Set --help option to get more information.')
    #     cli.exit()

    ctx.obj['DEBUG'] = debug

    if ctx.obj['DEBUG']:
        setup_logging('DEBUG')
    else:
        setup_logging('INFO')

    if not hostp:
        try:
            uri_tokens = get_config_uri(cfg)
            hostp = '{}:{}'.format(uri_tokens[1], uri_tokens[2])
            sche = uri_tokens[0]
        except SimplexException as e:
            e.print_error_resp()
            ctx.abort()

    ctx.obj['SCHEME'] = sche
    ctx.obj['HOSTPORT'] = hostp
    ctx.obj['JSONRESP'] = jrep

    click.secho('Building request to SAPI server authority {scheme}://{auth}'.format(scheme=sche,
                                                                                     auth=hostp), fg='white')

    _logger.debug('Context variables: SCHEME = {}, HOSTPORT = {}, JSONRESP = {}, '.format(ctx.obj['SCHEME'],
                                                                                          ctx.obj['HOSTPORT'],
                                                                                          ctx.obj['JSONRESP']))


@cli.command(name='circuit')
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_circuit_mfields,
              help='Specify the field to be match')
@click.option('-mr', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
@click.option('-c', '--command', 'comm', type=str, default='', autocompletion=get_circuit_cmds,
              help='Set the command')
@click.option('-ss', '--show-secret', 'shsecret', flag_value=True,
              help='Show secret circuit-related data')
@click.option('-a', '--attach-to', 'trid', default=0, type=int,
              help='Attach transport entry id to matched circuit\'s list')
@click.pass_context
def circuit(ctx, ident, mfld, mexp, comm, shsecret, trid):
    """Set a parameter on a list of circuit entries"""

    click.secho('Setting command on a list of circuits')

    """The keys of options dict come from REQ_KEYS"""

    opt = {
            "type": REF_SDU_VALUES['CircuitSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp,  # regular expression
            "show-secret": shsecret,
            "cmd": comm,
            "tid": trid
        }

    try:
        _stl = CircuitTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.set()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()


"""
@cli.command(name='carrier')
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_carrier_mfields,
              help='Specify the field to be match')
@click.option('-mr', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
@click.option('-n', '--name', 'name', type=str, required=True,
              help='Specify the name of the carrier')
@click.pass_context
def carrier(ctx, ident, mfld, mexp, name):
    # Set a list of carrier entries

    click.secho('Setting on a list of carriers')

    _data = {
        "name": name
    }

    opt = {
            "type": REF_SDU_VALUES['CarrierSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp,  # regular expression
            "data": _data
        }

    try:
        _stl = CarrierTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.set()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()

@cli.command(name='transport')
@click.option('-id', '--ident', 'ident', default=0, type=int,
              help='Specify referenced context entry id')
@click.option('-mf', '--match-field', 'mfld', type=str, default='', autocompletion=get_transport_mfields,
              help='Specify the field to be match')
@click.option('-mr', '--match-expr', 'mexp', type=str, default='',
              help='Specify the regular expression that must match the field\'s values')
@click.pass_context
def transport(ctx, ident, mfld, mexp):
    # Set a list of transport entries

    click.secho('Deleting a list of transport entries')

    opt = {
            "type": REF_SDU_VALUES['TransportSapiType'],
            "id": ident,  # This field does the trick if it is greater than 0
            "match-field": mfld,
            "match-expr": mexp  # regular expression
        }

    try:
        _stl = TransportTransaction(hostport=ctx.obj['HOSTPORT'], scheme=ctx.obj['SCHEME'], options=opt)
        _stl.set()
    except SimplexHTTPError as e:
        e.print_error_resp(sresp=ctx.obj['JSONRESP'])
    except SimplexException as f:
        f.print_error_resp()
        ctx.abort()
"""

if __name__ == '__main__':
    cli(obj={})
