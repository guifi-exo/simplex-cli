try:
    from importlib_metadata import metadata
    __user_agent__ = metadata(__name__)['Name']
    __version__ = metadata(__name__)['Version']
    pass

except ImportError(metadata):
    __user_agent__ = 'undefined'
    __version__ = 'undefined'

__cli_name__ = __name__
