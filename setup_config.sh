#!/bin/sh -e

# Allow using relative links
cd "$(dirname "${0}")"

SPXC_DIR="$HOME/.spxc"
mkdir -p "${SPXC_DIR}"

SPXC_AUTOCOMPLETE="${SPXC_DIR}"/spxc-complete.bash
echo "  Installing autocomplete in ${SPXC_AUTOCOMPLETE}"
echo '    adding spxlist'
_SPXLIST_COMPLETE=bash_source spxlist      > "${SPXC_AUTOCOMPLETE}"
echo '    adding spxset'
_SPXSET_COMPLETE=bash_source spxset       >> "${SPXC_AUTOCOMPLETE}"
echo '    adding spxcreate'
_SPXCREATE_COMPLETE=bash_source spxcreate >> "${SPXC_AUTOCOMPLETE}"
echo '    adding spxdelete'
_SPXDELETE_COMPLETE=bash_source spxdelete >> "${SPXC_AUTOCOMPLETE}"

echo "  Copy config files to user's home"
DATA_DIR="src/simplex_cli/data"
cp -vn "${DATA_DIR}"/spxc.cfg            "${SPXC_DIR}"
cp -vn "${DATA_DIR}"/transp_default.json "${SPXC_DIR}"

echo "\nSettings setup complete!\n"
BASHRC_LINE=". ${SPXC_AUTOCOMPLETE}"
if ! grep -qs "^${BASHRC_LINE}" "$HOME/.bashrc"; then
  cat <<EOF
Please add to .bashrc the following line:

${BASHRC_LINE}

EOF
fi
