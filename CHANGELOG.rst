=========
Changelog
=========

Version 1.0.3
=============

- New location for default configuration files and automation procedure
- Enabling autocomplete functions for bash by shell script during installation procedure

Version 1.0.2
=============

- Fixed a missing PayLoad SDU key

Version 1.0.1
=============

- Fixed improper catch of broad exceptions when get() method is invoked

Version 1.0.0
=============

- Rewritten the main part of the code structure based on new HRL and STL functional layers
- Replaced the original only tool by a tool's suite (spxcreate, spxlist, spxset and spxdelete)
- Added autocomplete features in match-field available values
- Added transport data on circuit table print function
- Defined a new TRBE format to support new bearing services i.e. Orange
- This release is not compatible with former releases


Version 0.4.1
=============

- Show a preview of the carrier before to add a transport entry
- Fixed incorrect value-match filtering if a None is returned
- Fixed improper data type processing in PATCH and POST basic functions
- Fixed adding a new subscriber does not create any circuit


Version 0.4.0
=============

- Implemented basic data structures of transport features: enable tacking JSON files
- Fixed not possible to set up circuit's state to disable
- Increasing defensive coding: exception control in case of malformed JSON responses
- Full implementation of transport management features. New common set command
- Add carrier features. Code refactoring of Requests module
- New filtered data printing based on regular expressions
- Changed default transport filename

Version 0.3.1
=============

- Fixed not loading configuration file in proper location when --config-file options invoked
- Fixed passing correct HTTP scheme flag
- Fixed incorrect format of URI endpoints when ident=0
- Added option to append management circuit to a new subscriber
- Enabled support to pytest

Version 0.3.0
=============

- Declared console_scripts endpoint in order to build/install the package with Pyscaffold
- Improved integration with pkg_resources: script name and base version tag
- Added missing header field in POST method building. Improved documentation
- Installation procedure added in documentation. Enabled installation of packages dependences from setup.py
- Added reading configuration file features, filenames and import processes
- New URI sheme selection option from command common arguments
- Autocomnplete and configuration files added during installation process
- Modified autocompletion script file name

Version 0.2.0
=============

- Code refactoring related with Request classes
- Added support to enable/disable state of circuits according to new SAPI feature
- Control of showing circuit's secrets
- Added catching decoding exception if no JSON format is retrieved
- Fixed exception control process if incomplete or unformatted JSON error is retrieved
- Added support to send disconnect packet to NAS according to new SAPI feature

Version 0.1.1
=============

- Added new command options to show JSON format response data
- Filtering by subscriber id when list, add and delete commands are invoked
- Reordening tracking messages and other header fields
- Fixed the processing procedure in case code 200 messages are received but with error content
- Improved table's look and network error processing

Version 0.1.0
=============

- Initial version just with few features